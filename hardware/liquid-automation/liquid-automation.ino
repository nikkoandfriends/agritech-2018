#define VALVE_PIN 5
#define TRIGGER 10
#define ECHO 11
#define TOGGLE_VALVE "TOGGLE_VALVE"
#define INQUIRE_WATER_LEVEL "INQUIRE_WATER_LEVEL"
#define INQUIRE_VALVE_STATUS "INQUIRE_VALVE_STATUS"
#define DRAIN_WATER_UNTIL "DRAIN_WATER_UNTIL "
#define FULL_TANK_LEVEL 0.05
#define EMPTY_TANK_LEVEL 0.30
#define DEBUG 1

float distance = 0;
bool valveStatus = 0;

void setup() {
  Serial.begin(9600);
  pinMode(VALVE_PIN, OUTPUT);
  Serial.println("===MCU READY===");
  delay(1000);
}

void loop() {
  if (Serial.available()) {
    String input = readSerialString();
    input.trim();
    Serial.print("INPUT: ");
    Serial.println(input);
    delay(1000);

    if (input.equals(TOGGLE_VALVE)) {
      toggleValve();
    }
    if (input.equals(INQUIRE_WATER_LEVEL)) {
      inquireWaterLevel();
    }
    if (input.equals(INQUIRE_VALVE_STATUS)) {
      inquireValveStatus();
    }
    if (input.startsWith(DRAIN_WATER_UNTIL)) {
      input.replace(DRAIN_WATER_UNTIL, "");
      if (DEBUG) {
        Serial.print("drain water percentage: ");
        Serial.println(input.toInt());
      }
      float level = mapValue(input.toInt(), 0, 100, EMPTY_TANK_LEVEL, FULL_TANK_LEVEL);
      if (DEBUG) {
        Serial.print("close valve if water level is: ");
        Serial.println(level);
      }
      inquireWaterLevel();
      toggleValve();
      while (distance <= level) {
        inquireWaterLevel();
      }
      toggleValve();
    }
  }
}

void inquireValveStatus() {
  Serial.print("VALVE_STATUS: ");
  Serial.println(valveStatus);
}

void inquireWaterLevel() {
  digitalWrite(TRIGGER, LOW);
  delayMicroseconds(5);
  digitalWrite(TRIGGER, HIGH);
  delayMicroseconds(10);
  distance = pulseIn(ECHO, HIGH);
  distance = distance * 0.0001657;

  if (DEBUG) {
    Serial.print("distance: ");
    Serial.println(distance);
  }

  if (distance > 1) {
    return;
  }

  float waterLevel = mapValue(distance, EMPTY_TANK_LEVEL, FULL_TANK_LEVEL, 0, 100);
  if (waterLevel > 100) {
    waterLevel = 100;
  } else if (waterLevel < 0) {
    waterLevel = 0;
  }
  Serial.print("WATER_LEVEL: ");
  Serial.println(waterLevel);
  delay(500);
}

void toggleValve() {
  if (valveStatus == 0) {
    Serial.println("Opening valve...");
    digitalWrite(VALVE_PIN, HIGH);
    valveStatus = 1;
  } else {
    Serial.println("Closing valve...");
    digitalWrite(VALVE_PIN, LOW);
    valveStatus = 0;
  }
  Serial.print("VALVE_STATUS: ");
  Serial.println(valveStatus);
  delay(500);
}

float mapValue(float x, float in_min, float in_max, float out_min, float out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

String readSerialString() {
  String rxData = "";

  while (true) {
    if (Serial.available()) {
      char data = Serial.read();

      if (data == '^') {
        break;
      } else {
        rxData = rxData + data;
      }
    }
  }

  return rxData;
}
