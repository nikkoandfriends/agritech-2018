package com.agritech.patubig.marketplace;

import com.agritech.patubig.IntegrationTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
public class ItemServiceTest extends IntegrationTest {

  @Autowired
  private ItemService itemService;

  @Test
  public void saveItemTest() {
    final Item item1 = new Item();
    item1.setDescription("Movento is a revolutionary insecticide/nematicide, offering truly unique two-way movement both upward and downward within plant tissue to find and control even hidden pests. It offers broad-spectrum control of many sucking pests, and is an important addition to integrated pest management programs.");
    item1.setName("Movento");
    item1.setType(ItemType.PESTICIDE);
    item1.setSupplier("Farmer's Market");
    item1.setPrice(1490.43);
    final Item savedItem1 = itemService.save(item1);
    Assert.assertNotNull(savedItem1);

    final Item item2 = new Item();
    item2.setDescription("ivanto Insecticide targets key damaging pests and helps safeguard beneficial insects to preserve the overall health of plants and protect crop investments.");
    item2.setName("Sivanto");
    item2.setType(ItemType.PESTICIDE);
    item1.setSupplier("Farmer's Market");
    item2.setPrice(1599.80);
    final Item savedItem2 = itemService.save(item2);
    Assert.assertNotNull(savedItem2);

    final Item item3 = new Item();
    item3.setDescription("Oberon Insecticide offers long-lasting residual activity against all mite life stages in corn, vegetables & cotton. Very effective against whitefly & psyllid nymphs");
    item3.setName("Oberon");
    item3.setType(ItemType.PESTICIDE);
    item1.setSupplier("Farmer's Market");
    item3.setPrice(999.50);
    final Item savedItem3 = itemService.save(item3);
    Assert.assertNotNull(savedItem3);

    final Item item4 = new Item();
    item4.setDescription("Bayer Requiem Insecticide provides flexibility, performance and peace of mind in the fight against sucking pests on high-value fruit and vegetable crops.");
    item4.setName("Bayer Requiem");
    item4.setType(ItemType.PESTICIDE);
    item1.setSupplier("Farmer's Market");
    item4.setPrice(760);
    final Item savedItem4 = itemService.save(item4);
    Assert.assertNotNull(savedItem4);

    final Item item5 = new Item();
    item5.setDescription("Velum One is a novel nematicide with fungicidal activity for use on almonds, tomatoes, peppers, strawberries, brassicas and cucurbits to help improve yield and quality.");
    item5.setName("Velum One");
    item5.setType(ItemType.PESTICIDE);
    item1.setSupplier("Farmer's Market");
    item5.setPrice(550.80);
    final Item savedItem5 = itemService.save(item5);
    Assert.assertNotNull(savedItem5);
  }

  @Test
  public void findAllTest() {
    final Item item1 = new Item();
    item1.setDescription("Movento is a revolutionary insecticide/nematicide, offering truly unique two-way movement both upward and downward within plant tissue to find and control even hidden pests. It offers broad-spectrum control of many sucking pests, and is an important addition to integrated pest management programs.");
    item1.setName("Movento");
    item1.setType(ItemType.PESTICIDE);
    item1.setSupplier("Farmer's Market");
    item1.setPrice(1490.43);
    final Item savedItem1 = itemService.save(item1);
    Assert.assertNotNull(savedItem1);
    final List<Item> items = itemService.findAll();
    Assert.assertNotNull(items);

  }
}