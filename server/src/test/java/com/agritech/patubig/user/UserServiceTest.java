package com.agritech.patubig.user;

import com.agritech.patubig.IntegrationTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
public class UserServiceTest extends IntegrationTest {

  @Autowired
  private UserService userService;

  @Test
  public void saveUserTest() {
    final User user = new User();
    user.setUsername("test@email.com");
    user.setPassword("admin");
    user.setContactNumber("091243567");
    user.setFirstName("Chuck");
    user.setMiddleName("The Legend");
    user.setLastName("Norris");
    user.setAddress("Middle Earth");
    final User savedUser = userService.save(user);
    Assert.assertNotNull(savedUser);
  }
}