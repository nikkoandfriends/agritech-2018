package com.agritech.patubig.hardware.mcu;

import com.agritech.patubig.IntegrationTest;
import com.agritech.patubig.exception.HardwareException;
import com.agritech.patubig.util.SleepUtil;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

public class McuServiceIntegrationTest extends IntegrationTest {

    @Autowired
    private McuService mcuService;

    @Test
    @Ignore
    public void toggleValveTest() throws HardwareException {
        mcuService.toggleValve();
    }

    @Test
    @Ignore
    public void getWaterLevelTest() throws HardwareException {
        int waterLevel = mcuService.getWaterLevel();
        System.out.println(waterLevel);
        Assert.isTrue(waterLevel < 100 && waterLevel > 0, "");
    }

    @Test
    @Ignore
    public void getValveStatusTest() throws HardwareException {
        int valveStatus = mcuService.getValveStatus();
        System.out.println(valveStatus);
        Assert.isTrue(valveStatus == 0 || valveStatus == 1, "");
    }

    @Test
    @Ignore
    public void drainWaterUntilTest() throws HardwareException {
        int percentage = 15;
        mcuService.drainWaterUntil(percentage);
        SleepUtil.sleep(20_000);
    }
}
