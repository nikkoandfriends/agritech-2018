package com.agritech.patubig.hardware.serial;

import com.agritech.patubig.IntegrationTest;
import com.agritech.patubig.exception.HardwareException;
import com.agritech.patubig.util.SleepUtil;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.Executor;

public class SerialLinkServiceIntegrationTest extends IntegrationTest {

    @Autowired
    private SerialLinkService serialLinkService;
    @Autowired
    private Executor executor;

    @Test
    @Ignore
    public void test() {
        waitForSerialConnection();
        runValveOpeningOnBackground();
        readSerialData();
    }

    private void readSerialData() {
        while (true) {
            String data = serialLinkService.readData(3_000);
            System.out.println(data);
        }
    }

    private void runValveOpeningOnBackground() {
        executor.execute(() -> {
            while (true) {
                try {
                    serialLinkService.transmitData("TOGGLE_VALVE^");
                } catch (HardwareException e) {
                    e.printStackTrace();
                }

                SleepUtil.sleep(2000);
            }
        });
    }

    private void waitForSerialConnection() {
        while (!serialLinkService.isLinked()) {
            try {
                serialLinkService.linkUp();
            } catch (HardwareException e) {
                e.printStackTrace();
            }
        }
    }
}
