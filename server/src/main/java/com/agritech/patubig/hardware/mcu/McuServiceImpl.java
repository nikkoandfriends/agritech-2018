package com.agritech.patubig.hardware.mcu;

import com.agritech.patubig.exception.HardwareException;
import com.agritech.patubig.hardware.serial.SerialLinkService;
import com.agritech.patubig.util.SleepUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
class McuServiceImpl implements McuService {

    private static final String WATER_LEVEL = "WATER_LEVEL: ";
    private static final String VALVE_STATUS = "VALVE_STATUS: ";
    private static final String INQUIRE_WATER_LEVEL = "INQUIRE_WATER_LEVEL^";
    private static final String INQUIRE_VALVE_STATUS = "INQUIRE_VALVE_STATUS^";

    @Autowired
    private SerialLinkService serialLinkService;

    @Override
    public void toggleValve() throws HardwareException {
        issueAnErrorIfMcuIsNotConnected();
        serialLinkService.transmitData("TOGGLE_VALVE^");
    }

    @Override
    public int getWaterLevel() throws HardwareException {
        issueAnErrorIfMcuIsNotConnected();
        int waterLevel = -1;

        while (waterLevel == -1) {
            String value = inquireToMcu(INQUIRE_WATER_LEVEL, WATER_LEVEL, HardwareException.ERROR_WATER_LEVEL_INQUIRY);
            try {
                waterLevel = (int) Double.parseDouble(value);
            } catch (NumberFormatException e) {
                log.error("{}", e);
                waterLevel = -1;
            }
        }
        return waterLevel;
    }

    @Override
    public int getValveStatus() throws HardwareException {
        issueAnErrorIfMcuIsNotConnected();
        int valveStatus = -1;

        while (valveStatus == -1) {
            try {
                String value = inquireToMcu(INQUIRE_VALVE_STATUS, VALVE_STATUS, HardwareException.ERROR_VALVE_STATUS_INQUIRY);
                valveStatus = (int) Double.parseDouble(value);
            } catch (NumberFormatException e) {
                valveStatus = -1;
                log.error("{}", e);
            }
        }
        return valveStatus;
    }

    @Override
    public void drainWaterUntil(int percentage) throws HardwareException {
        issueAnErrorIfMcuIsNotConnected();
        serialLinkService.transmitData("DRAIN_WATER_UNTIL " + percentage + "^");
    }

    private String inquireToMcu(String command, String prefix, String errorMessage) throws HardwareException {
        serialLinkService.transmitData(command);
        String message = "";
        int tries = 0;
        while (!message.startsWith(prefix) && tries < 10) {
            message = serialLinkService.readData(3000);
            log.info("inquiry message reply: " + message);
            tries++;
        }
        if (tries == 10) {
            throw new HardwareException(errorMessage);
        }
        return message.replace(prefix, "");
    }

    private void issueAnErrorIfMcuIsNotConnected() throws HardwareException {
        if (!serialLinkService.isLinked()) {
            throw new HardwareException(HardwareException.MCU_NOT_DETECTED);
        }
    }
}
