package com.agritech.patubig.hardware.mcu;

import lombok.Data;

/**
 * @author John Matthew Labtic <matthewlabtic@gmail.com>
 */
@Data
public class ValveRequest {
	private int waterToConsume;
	private int timeStartMin;
	private int timeStartHour;
	private long intervalInMin;
}
