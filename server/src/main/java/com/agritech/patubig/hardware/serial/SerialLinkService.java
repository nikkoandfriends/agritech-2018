package com.agritech.patubig.hardware.serial;

import com.agritech.patubig.exception.HardwareException;
import jssc.SerialPort;

public interface SerialLinkService {

    boolean isLinked();

    void monitorConnectivity();

    String linkUp() throws HardwareException;
	void linkDown() throws HardwareException;
	String scanPorts();
	String readData(long time);
	void transmitData(String msg) throws HardwareException;
	SerialPort getSerialPort();
}
