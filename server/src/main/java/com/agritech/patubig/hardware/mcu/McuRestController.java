package com.agritech.patubig.hardware.mcu;

import com.agritech.patubig.exception.HardwareException;
import com.agritech.patubig.util.SleepUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.Executor;

/**
 * @author John Matthew Labtic <matthewlabtic@gmail.com>
 */
@RestController
@RequestMapping("/api/hardware")
@Slf4j
public class McuRestController {

    private final McuService mcuService;

    public McuRestController(McuService mcuService) {
        this.mcuService = mcuService;
    }

    @GetMapping("/status")
    public ResponseEntity<?> getStatus() {
        try {
            return ResponseEntity.ok(getMcuStatus());
        } catch (HardwareException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/valve/toggle")
    public ResponseEntity<?> toggleValve() {
        try {
            mcuService.toggleValve();
        } catch (HardwareException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok().build();
    }

    @PostMapping("manualtoggle")
    public ResponseEntity<?> manualToggleValve(@RequestBody ValveRequest request) {
        try {
            int initialWaterLevel = mcuService.getWaterLevel();
            if (mcuService.getValveStatus() == 0) {
                if (initialWaterLevel < request.getWaterToConsume()) {
                    throw new IllegalArgumentException("Water level not enough");
                }

                mcuService.drainWaterUntil(initialWaterLevel - request.getWaterToConsume());
                SleepUtil.sleep(60_000);
            } else {
                mcuService.toggleValve();
            }
            return ResponseEntity.ok(mcuService.getWaterLevel());
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e);
        }
    }

    private ValveResponse getMcuStatus() throws HardwareException {
        ValveResponse response = new ValveResponse();
        response.setValveStatus(mcuService.getValveStatus());
        response.setWaterLevel(mcuService.getWaterLevel());
        return response;
    }
}
