package com.agritech.patubig.hardware.mcu;

import com.agritech.patubig.exception.HardwareException;

public interface McuService {

    void toggleValve() throws HardwareException;

    int getWaterLevel() throws HardwareException;

    int getValveStatus() throws HardwareException;

    void drainWaterUntil(int percentage) throws HardwareException;
}
