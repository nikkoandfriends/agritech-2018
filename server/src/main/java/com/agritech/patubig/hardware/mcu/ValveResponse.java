package com.agritech.patubig.hardware.mcu;

import lombok.Data;

/**
 * @author John Matthew Labtic <matthewlabtic@gmail.com>
 */
@Data
public class ValveResponse {

	private int waterLevel;
	private int valveStatus;
}
