package com.agritech.patubig.util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SleepUtil {

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            log.error("{}", e);
        }
    }
}
