package com.agritech.patubig.payment;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
@RestController
@RequestMapping("/api")
public class UnionBankPaymentRestController {

  @Value("${app.unionbank.uri}")
  private String unionBankUri;
  @Value("${app.unionbank.clientid}")
  private String unionBankClientId;
  @Value("${app.unionbank.secret}")
  private String unionBankSecret;
  @Value("${app.unionbank.redirecturi}")
  private String unionBankRedirectUri;
  @Value("${app.unionbank.tokenuri}")
  private String unionBankTokenUri;


  private final RestTemplate restTemplate;

  public UnionBankPaymentRestController(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @GetMapping("/login")
  public void redirectPaymentLogin(HttpServletResponse httpServletResponse) throws IOException {
    String redirectLink = String.format("https://api-uat.unionbankph.com/partners/sb/convergent/v1/oauth2/authorize?client_id=%s&scope=transfers&response_type=code&redirect_uri=%s",unionBankClientId, unionBankRedirectUri);
    httpServletResponse.sendRedirect(redirectLink);
  }

  @GetMapping("/oauth/redirect")
  public JsonNode oauthRedirect(@RequestParam String code) {
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
    httpHeaders.add("accept", "text/html");
    MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
    map.add("client_id", unionBankClientId);
    map.add("code", code);
    map.add("redirect_uri", unionBankRedirectUri);
    map.add("grant_type", "authorization_code");
    HttpEntity httpEntity = new HttpEntity(map, httpHeaders);
    final ResponseEntity<JsonNode> response = restTemplate.exchange(unionBankTokenUri, HttpMethod.POST, httpEntity, JsonNode.class);
    return response.getBody();
  }


}