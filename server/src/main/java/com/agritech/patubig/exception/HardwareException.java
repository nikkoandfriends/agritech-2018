package com.agritech.patubig.exception;

public class HardwareException extends Exception {

    public static final String MCU_NOT_DETECTED = "Microcontroller not detected.";
    public static final String MCU_CONNECT_FAILURE = "Microcontroller connection failure.";
    public static final String MCU_DISCONNECT_FAILURE = "Microcontroller disconnection failure.";
    public static final String MCU_DATA_TRANSMIT_FAILURE = "Microcontroller data transmit failure.";
    public static final String ERROR_WATER_LEVEL_INQUIRY = "Error performing inquiry on water level.";
    public static final String ERROR_VALVE_STATUS_INQUIRY = "Error performing inquiry on valve status";

    public HardwareException(String message) {
        super(message);
    }
}
