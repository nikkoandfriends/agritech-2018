package com.agritech.patubig.shared;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
@Data
public abstract class BaseModel {

  private String id;
  @CreatedDate
  private long createdDate;
  @LastModifiedDate
  private long lastDateModified;
}
