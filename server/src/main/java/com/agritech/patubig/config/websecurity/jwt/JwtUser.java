package com.agritech.patubig.config.websecurity.jwt;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Michael Ryan A. Paclibar <michael@satellite.com.ph>
 */
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class JwtUser implements UserDetails {

  private String id;
  private String email;
  private String password;
  private String firstName;
  private String middleName;
  private String lastName;
  private String contactNumber;
  private String adddress;
  private boolean active;
  private List<String> roles;

  void clearCredentials() {
    this.password = null;
  }

  public String getFullName() {
    StringBuilder sb = new StringBuilder();
    sb.append(this.lastName);
    sb.append(" ");
    sb.append(this.firstName);
    return sb.toString();
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return this.roles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
  }

  @Override
  public String getPassword() {
    return this.password;
  }

  @Override
  public String getUsername() {
    return this.email;
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }
}
