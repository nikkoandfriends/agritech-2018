package com.agritech.patubig.config;

import com.agritech.patubig.hardware.serial.SerialLinkService;
import com.agritech.patubig.marketplace.ItemService;
import com.agritech.patubig.util.SleepUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.mobile.device.DeviceHandlerMethodArgumentResolver;
import org.springframework.mobile.device.DeviceResolverHandlerInterceptor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executor;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
@Configuration
@Slf4j
public class AppConfig implements WebMvcConfigurer {

  @Autowired
  private SerialLinkService serialLinkService;
  @Autowired
  private ItemService itemService;
  @Value("${mcu.startup.run}")
  private boolean mcuConnectionShouldBeEstablishedOnStartup;

  @Bean
  public CommandLineRunner onAppStart() {
    return args -> {
      itemService.generateDummyData();
      if (mcuConnectionShouldBeEstablishedOnStartup) {
        log.info("Establishing MCU connection...");
        serialLinkService.linkUp();
        SleepUtil.sleep(2000);
        if (serialLinkService.isLinked()) {
          log.info("Monitoring MCU connectivity...");
          serialLinkService.monitorConnectivity();
        }
      }
    };
  }

  @Bean
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }

  @Bean
  public DeviceResolverHandlerInterceptor
  deviceResolverHandlerInterceptor() {
    return new DeviceResolverHandlerInterceptor();
  }

  @Bean
  public DeviceHandlerMethodArgumentResolver
  deviceHandlerMethodArgumentResolver() {
    return new DeviceHandlerMethodArgumentResolver();
  }

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(deviceResolverHandlerInterceptor());
  }

  @Override
  public void addArgumentResolvers(
          List<HandlerMethodArgumentResolver> argumentResolvers) {
    argumentResolvers.add(deviceHandlerMethodArgumentResolver());
  }

  @EnableMongoAuditing
  @Configuration
  public static class MongoAuditConfig implements AuditorAware<String> {
    @Override
    public Optional<String> getCurrentAuditor() {
      return Optional.empty();
    }
  }

  @EnableSwagger2
  @Profile({"dev"})
  @Controller
  @Configuration
  public static class SwaggerConfig {

    @RequestMapping("/")
    public String redirectSwaggerMainPage() {
      return "redirect:/swagger-ui.html";
    }

    @Bean
    public Docket documentation() {
      return new Docket(DocumentationType.SWAGGER_2)
              .select()
              .apis(RequestHandlerSelectors.any())
              .paths(PathSelectors.regex("/api/.*"))
              .build().pathMapping("/")
              .apiInfo(metadata())
              .globalOperationParameters(getOperationParameters());
    }

    private List<Parameter> getOperationParameters() {
      return Collections.singletonList(new ParameterBuilder().name("Authorization").modelRef(new ModelRef("string")).parameterType("header").required(false).build());
    }

    private ApiInfo metadata() {
      return new ApiInfoBuilder()
              .title("Agritech Web Service API")
              .description("Use this documentation as a reference how to interact with app's API")
              .build();
    }
  }

  @Bean
  public Executor executor() {
    ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
    // uncomment for testing hardware
        threadPoolTaskExecutor.setKeepAliveSeconds(9999999);
        threadPoolTaskExecutor.setAwaitTerminationSeconds(99999);
    return threadPoolTaskExecutor;
  }
}
