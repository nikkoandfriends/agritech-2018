package com.agritech.patubig.config.websecurity.jwt;

import com.agritech.patubig.config.websecurity.CustomUserDetailService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
@RestController
public class JwtRestRecontroller {

  @Value("${app.jwt.header}")
  private String tokenHeader;

  private final AuthenticationManager authenticationManager;
  private final JwtTokenUtil jwtTokenUtil;
  private final CustomUserDetailService userDetailsService;

  @Autowired
  public JwtRestRecontroller(AuthenticationManager authenticationManager, JwtTokenUtil jwtTokenUtil, CustomUserDetailService userDetailsService) {
    this.authenticationManager = authenticationManager;
    this.jwtTokenUtil = jwtTokenUtil;
    this.userDetailsService = userDetailsService;
  }

  @RequestMapping(value = "/api/auth", method = RequestMethod.POST)
  @ApiOperation(value = "Authenticate User")
  public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest,
                                                     Device device) {

    final Authentication authentication = authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(),
                    authenticationRequest.getPassword()));

    SecurityContextHolder.getContext().setAuthentication(authentication);
    final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
    final String token = jwtTokenUtil.generateToken(userDetails, device);
    JwtAuthenticationResponse jwtAuthenticationResponse = new JwtAuthenticationResponse(token);
    return ResponseEntity.ok(jwtAuthenticationResponse);
  }

  @RequestMapping(value = "/api/auth/refresh", method = RequestMethod.GET)
  @ApiOperation(value = "Refresh JWT Token")
  public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
    String token = request.getHeader(tokenHeader);
    String username = jwtTokenUtil.getUsernameFromToken(token);
    UserDetails user = userDetailsService.loadUserByUsername(username);
    if (user != null && jwtTokenUtil.canTokenBeRefreshed(token)) {
      return ResponseEntity.ok(new JwtAuthenticationResponse(jwtTokenUtil.refreshToken(token)));
    }
    return ResponseEntity.badRequest().body(null);
  }
}
