package com.agritech.patubig.config.websecurity;

import com.agritech.patubig.config.websecurity.jwt.JwtUser;
import com.agritech.patubig.user.User;
import com.agritech.patubig.user.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
@Service
public class CustomUserDetailService implements UserDetailsService {

  private final UserRepository userRepository;

  public CustomUserDetailService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = userRepository.findByUsername(username);
    if (user == null) {
      throw new UsernameNotFoundException("User does not exists");
    }
    return new JwtUser().toBuilder()
            .active(user.isActive())
            .adddress(user.getAddress())
            .contactNumber(user.getContactNumber())
            .email(user.getUsername())
            .firstName(user.getFirstName())
            .middleName(user.getMiddleName())
            .lastName(user.getLastName())
            .id(user.getId())
            .password(user.getPassword())
            .roles(Arrays.asList(user.getRoles()))
            .build();
  }
}
