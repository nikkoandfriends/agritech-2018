package com.agritech.patubig.config.websecurity;

import com.agritech.patubig.config.websecurity.jwt.JwtAuthenticationEntryPoint;
import com.agritech.patubig.config.websecurity.jwt.JwtAuthenticationTokenFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
@Configuration
@EnableWebSecurity
public class CustomWebSecurity extends WebSecurityConfigurerAdapter {

  private static final String[] DEFAULT_ROLES = {"ROLE_ADMIN", "ROLE_USER"};

  private final CustomUserDetailService customUserDetailService;
  private final JwtAuthenticationEntryPoint unauthorizedHandler;

  public CustomWebSecurity(CustomUserDetailService customUserDetailService, JwtAuthenticationEntryPoint unauthorizedHandler) {
    this.customUserDetailService = customUserDetailService;
    this.unauthorizedHandler = unauthorizedHandler;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
            .csrf().disable()
            .exceptionHandling().authenticationEntryPoint(unauthorizedHandler)
            .and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .antMatcher("/api/**")
            .authorizeRequests()
            .antMatchers("/", "/api/auth/**", "/api/user/register", "/api/oauth/redirect","/api/login").permitAll()
            .antMatchers("/api/**").hasAnyAuthority(DEFAULT_ROLES)
            .anyRequest().authenticated();
    http.addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class);
    http.headers().cacheControl();

  }

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(customUserDetailService).passwordEncoder(passwordEncoder());
  }

  @Bean
  public JwtAuthenticationTokenFilter authenticationTokenFilterBean() {
    return new JwtAuthenticationTokenFilter();
  }

  @Bean
  PasswordEncoder passwordEncoder() {
    return PasswordEncoderFactories.createDelegatingPasswordEncoder();
  }

  @Override
  @Bean
  public AuthenticationManager authenticationManagerBean()
          throws Exception {
    return super.authenticationManagerBean();
  }

}
