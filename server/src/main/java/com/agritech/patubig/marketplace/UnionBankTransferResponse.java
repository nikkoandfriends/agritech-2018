package com.agritech.patubig.marketplace;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UnionBankTransferResponse {

  private String tranId;
  private String createdAt;
  private String state;
  private String senderTransferId;

}
