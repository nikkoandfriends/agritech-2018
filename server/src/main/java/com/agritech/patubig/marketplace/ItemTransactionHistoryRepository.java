package com.agritech.patubig.marketplace;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
public interface ItemTransactionHistoryRepository extends MongoRepository<ItemTransactionHistory, String> {

  List<ItemTransactionHistory> findAllByUserId(String userId);
}
