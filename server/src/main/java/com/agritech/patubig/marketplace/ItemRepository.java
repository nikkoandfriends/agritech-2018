package com.agritech.patubig.marketplace;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
public interface ItemRepository extends MongoRepository<Item, String> {
}
