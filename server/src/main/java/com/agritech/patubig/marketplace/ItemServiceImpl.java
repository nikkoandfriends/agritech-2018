package com.agritech.patubig.marketplace;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
@Service
public class ItemServiceImpl implements ItemService {

  private final ItemRepository itemRepository;
  private final ItemTransactionHistoryRepository itemTransactionHistoryRepository;

  public ItemServiceImpl(ItemRepository itemRepository, ItemTransactionHistoryRepository itemTransactionHistoryRepository) {
    this.itemRepository = itemRepository;
    this.itemTransactionHistoryRepository = itemTransactionHistoryRepository;
  }

  @Override
  public Item save(Item item) {
    return itemRepository.save(item);
  }

  @Override
  public List<Item> findAll() {
    return itemRepository.findAll();
  }

  @Override
  public void generateDummyData() {
    itemRepository.deleteAll();
    final Item item1 = new Item("Movento", "Movento is a revolutionary insecticide/nematicide, offering truly unique two-way movement both upward and downward within plant tissue to find and control even hidden pests. It offers broad-spectrum control of many sucking pests, and is an important addition to integrated pest management programs.", 1490.43, "Farmer's Market", true, ItemType.PESTICIDE);
    final Item item2 = new Item("Sivanto", "Sivanto Insecticide targets key damaging pests and helps safeguard beneficial insects to preserve the overall health of plants and protect crop investments.", 1599.80, "Farmer's Market", true, ItemType.PESTICIDE);
    final Item item3 = new Item("Oberon", "Oberon Insecticide offers long-lasting residual activity against all mite life stages in corn, vegetables & cotton. Very effective against whitefly & psyllid nymphs", 999.50, "Farmer's Market", true, ItemType.PESTICIDE);
    final Item item4 = new Item("Bayer Requiem", "Bayer Requiem Insecticide provides flexibility, performance and peace of mind in the fight against sucking pests on high-value fruit and vegetable crops.", 760, "Farmer's Market", true, ItemType.PESTICIDE);
    final Item item5 = new Item("Velum One", "Velum One is a novel nematicide with fungicidal activity for use on almonds, tomatoes, peppers, strawberries, brassicas and cucurbits to help improve yield and quality.", 550.80, "Farmer's Market", true, ItemType.PESTICIDE);

    final Item item6 = new Item("Miracle-Gro Liquid All Purpose Plant Food", "Produce healthier-looking plants with this Miracle-Gro Liquid All-Purpose Plant Food, 32 oz. It instantly gives them the nutrients they need to flourish. You can use this liquid fertilizer for flowers, vegetables, trees, shrubs, as well as houseplants. Just mix the concentrated formula with water and then feed your vegetation every 1-2 weeks for big, beautiful blooms and healthy deep green foliage. It's available in a convenient 32 fl oz bottle with a resealable measuring cap and a molded-in handle for ease of use.", 535.50, "Miracle Go", true, ItemType.FERTILIZER);
    final Item item7 = new Item("Urban Farm Fertilizers Liquid Lawn Fertilizer", "Liquid Lawn 10-1-2 from Urban Farm Fertilizers is a professional, custom-blended fertilizer for all lawns, pasture, and hay production. It is the only instant lawn fertilizer with both Calcium and Iron. All nutrition, including calcium, is instantly available for quick, lush growth. Perfect for quick and easy hose-end and boom sprayers. One gallon dilutes into 256 gallons of full-strength lawn fertilizer that will provide a foliar spray for up to 130,000 sq ft. Liquid Lawn also contains mycorrhizae, humic acid, kelp, bat guano, worm casts, and enzymes for root zone bio-activation.", 1501.54, "Urban Farm", true, ItemType.FERTILIZER);
    final Item item8 = new Item("Jobe's Organics All-Purpose Water-Soluble Fertilizer", "Ensure that your plants grow to their fullest potential with the help of Jobes Organic All Purpose Fertilizer. This environment-friendly blend is also water-soluble. The brands synergistic blend of Biozome, plus organic plant nutrients and beneficial microbial species are uniquely combined to enhance soil conditions, stimulate plant growth and boost resistance to disease, insects and drought. Jobes fertilizer is just what you need to support the full growth of your organic garden. It is also easy to use. Just measure the right dosage using the enclosed measuring spoon and apply to your plants by mixing it into a watering can or spray applicator. This water soluble fertilizer pack makes up to 30 gallons.", 534.91, "Jobe's", true, ItemType.FERTILIZER);
    final Item item9 = new Item("FoxFarm Tiger Bloom Liquid Concentrate Plant Fertilizer", "It's fast, it's powerful, and it's ready to make your plants pounce to life. FoxFarm Tiger Bloom Fertilizer uses a powerful mix of key nutrients to encourage abundant fruit, flower, and bud development. This concentrated liquid fertilizer is formulated with high phosphorus and plenty of nitrogen, as well as low pH to maintain micro-nutrients and longevity in storage. For use in both hydroponic and soil applications, start using 2-3 teaspoons mixed with a gallon of water at the first signs of flowering.", 918.00, "FoxFarm", true, ItemType.FERTILIZER);
    final Item item10 = new Item("Dr. Earth Organic & Natural Home Grown Tomato, Vegetable & Herb Fertilizer", "Dr. Earth Home Grown Tomato, Vegetable & Herb Fertilizer is formulated to feed plants gently to promote robust and productive vegetable gardens. Our Home Grown Tomato, Vegetable & Herb Fertilizer is great for all types of vegetable gardens, both summer and winter, rooting and flowering gardens.", 973.65, "Dr. Earth Organic", true, ItemType.FERTILIZER);

    itemRepository.saveAll(Arrays.asList(item1, item2, item3, item4, item5, item6, item7, item8, item9, item10));
  }

  @Override
  public void saveItemTransactionHistory(ItemTransactionHistory itemTransactionHistory) {
    itemTransactionHistoryRepository.save(itemTransactionHistory);
  }

  @Override
  public List<ItemTransactionHistory> findAllItemTransactionHistory(String userId) {
    return itemTransactionHistoryRepository.findAllByUserId(userId);
  }
}
