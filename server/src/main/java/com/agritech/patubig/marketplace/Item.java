package com.agritech.patubig.marketplace;

import com.agritech.patubig.shared.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Item extends BaseModel {

  private String name;
  private String description;
  private double price;
  private String supplier;
  private boolean active = true;
  private ItemType type;

}
