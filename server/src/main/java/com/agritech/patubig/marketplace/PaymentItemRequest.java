package com.agritech.patubig.marketplace;

import lombok.Data;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
@Data
public class PaymentItemRequest {
  private String accessToken;
  private Item item;
  private String userId;
  private String fullName;
  private int quantity;
}
