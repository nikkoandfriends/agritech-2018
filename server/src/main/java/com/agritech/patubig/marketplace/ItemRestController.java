package com.agritech.patubig.marketplace;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
@RestController
@RequestMapping("/api/item")
public class ItemRestController {

  private static final String SANDBOX_PARTNER_ID = "01bbb51e-1e6c-4bd4-af9c-450957522aac";
  // Sandbox account we created that stands as the merchant.
  private static final String SANDBOX_ACCOUNT_NO = "100617191075";


  @Value("${api.unionbank.onlinetransferuri}")
  private String unionBankOnlineTransferUri;
  @Value("${app.unionbank.clientid}")
  private String unionBankClientId;
  @Value("${app.unionbank.secret}")
  private String unionBankClientSecret;

  private final ItemService itemService;
  private final RestTemplate restTemplate;

  public ItemRestController(ItemService itemService, RestTemplate restTemplate) {
    this.itemService = itemService;
    this.restTemplate = restTemplate;
  }

  @GetMapping("/list")
  public List<Item> findAll() {
    return itemService.findAll();
  }

  @GetMapping("/itemtransactions")
  public List<ItemTransactionHistory> findAllItemTransactionHistory(@RequestParam String userId) {
    return itemService.findAllItemTransactionHistory(userId);
  }

  @PostMapping("/paymerchant")
  public void buyItem(@RequestBody PaymentItemRequest paymentItemRequest) {
    HttpHeaders httpHeaders = new HttpHeaders();
    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
    httpHeaders.add("x-ibm-client-id", unionBankClientId);
    httpHeaders.add("x-ibm-client-secret", unionBankClientSecret);
    httpHeaders.add("Authorization", "Bearer " + paymentItemRequest.getAccessToken());
    httpHeaders.add("x-partner-id", SANDBOX_PARTNER_ID);

    TimeZone tz = TimeZone.getTimeZone("UTC");
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
    df.setTimeZone(tz);
    String isoDate = df.format(new Date());

    final PaymentRequest paymentRequest = new PaymentRequest(UUID.randomUUID().toString().substring(0, 10), isoDate, SANDBOX_ACCOUNT_NO, "Payment", "");
    paymentRequest.setAmount(new PaymentAmount("PHP", String.valueOf(paymentItemRequest.getItem().getPrice())));
    paymentRequest.setInfo(Arrays.asList(new PaymentInfo(1, "Recipient", paymentItemRequest.getFullName())));

    HttpEntity httpEntity = new HttpEntity(toJson(paymentRequest), httpHeaders);
    final ResponseEntity<UnionBankTransferResponse> response = restTemplate.exchange(unionBankOnlineTransferUri, HttpMethod.POST, httpEntity, UnionBankTransferResponse.class);
    if (response.getStatusCode().is2xxSuccessful()) {
      final ItemTransactionHistory itemTransactionHistory = new ItemTransactionHistory();
      itemTransactionHistory.setUserId(paymentItemRequest.getUserId());
      itemTransactionHistory.setFullName(paymentItemRequest.getFullName());
      itemTransactionHistory.setQuantity(paymentItemRequest.getQuantity());
      itemTransactionHistory.setUnionBankTransfer(response.getBody());
      itemTransactionHistory.setItem(paymentItemRequest.getItem());
      itemService.saveItemTransactionHistory(itemTransactionHistory);
    }
  }

  private String toJson(Object data) {
    ObjectMapper objectMapper = new ObjectMapper();
    try {
      return objectMapper.writeValueAsString(data);
    } catch (JsonProcessingException e) {
      return "";
    }
  }

  @Getter
  @Setter
  private static class PaymentRequest {
    private String senderTransferId;
    private String transferRequestDate;
    private String accountNo;
    private PaymentAmount amount;
    private String remarks;
    private String particulars;
    private List<PaymentInfo> info;

    public PaymentRequest() {
    }

    public PaymentRequest(String senderTransferId, String transferRequestDate, String accountNo, String remarks, String particulars) {
      this.senderTransferId = senderTransferId;
      this.transferRequestDate = transferRequestDate;
      this.accountNo = accountNo;
      this.remarks = remarks;
      this.particulars = particulars;
    }
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  private static class PaymentInfo {
    private int index;
    private String name;
    private String value;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  private static class PaymentAmount {
    private String currency;
    private String value;
  }

}
