package com.agritech.patubig.marketplace;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
public enum ItemType {
  PESTICIDE, FERTILIZER
}
