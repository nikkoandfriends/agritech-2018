package com.agritech.patubig.marketplace;

import java.util.List;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
public interface ItemService {
  Item save(Item item);

  List<Item> findAll();

  void generateDummyData();

  void saveItemTransactionHistory(ItemTransactionHistory itemTransactionHistory);

  List<ItemTransactionHistory> findAllItemTransactionHistory(String userId);
}
