package com.agritech.patubig.marketplace;

import com.agritech.patubig.shared.BaseModel;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
@Document
@Data
public class ItemTransactionHistory extends BaseModel {

  private UnionBankTransferResponse unionBankTransfer;
  private Item item;
  private int quantity;
  private String userId;
  private String fullName;
}
