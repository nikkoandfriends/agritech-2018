package com.agritech.patubig.user;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
public enum Roles {
  ROLE_USER, ROLE_ADMIN
}
