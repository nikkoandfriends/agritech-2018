package com.agritech.patubig.user;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
@RestController
@RequestMapping("/api/user")
public class UserRestController {

  private final UserService userService;

  public UserRestController(UserService userService) {
    this.userService = userService;
  }

  @PostMapping("/register")
  public User registerUser(@RequestBody UserRequest userRequest) {
    return userService.save(new User(userRequest.getUsername(), userRequest.getPassword(), userRequest.getFirstName(), userRequest.getMiddleName(), userRequest.getLastName(), userRequest.getAddress(), userRequest.getContactNumber()));
  }

}
