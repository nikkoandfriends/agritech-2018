package com.agritech.patubig.user;

import com.agritech.patubig.shared.BaseModel;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
@Getter
@Setter
@EqualsAndHashCode(callSuper = true, doNotUseGetters = true)
@ToString(callSuper = true, doNotUseGetters = true)
@Document
@NoArgsConstructor
public class User extends BaseModel {

  private String username;
  private String password;
  private String firstName;
  private String middleName;
  private String lastName;
  private String address;
  private String contactNumber;
  private String[] roles = new String[]{Roles.ROLE_USER.toString()};
  private boolean active = true;

  public User(String username, String password, String firstName, String middleName, String lastName, String address, String contactNumber) {
    this.username = username;
    this.password = password;
    this.firstName = firstName;
    this.middleName = middleName;
    this.lastName = lastName;
    this.address = address;
    this.contactNumber = contactNumber;
  }
}
