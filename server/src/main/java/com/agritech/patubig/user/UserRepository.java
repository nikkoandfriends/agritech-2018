package com.agritech.patubig.user;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
public interface UserRepository extends MongoRepository<User, String> {
  User findByUsername(String username);
}
