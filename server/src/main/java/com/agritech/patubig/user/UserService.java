package com.agritech.patubig.user;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
public interface UserService {

  User save(User user);

  User findByUsername(String username);
}
