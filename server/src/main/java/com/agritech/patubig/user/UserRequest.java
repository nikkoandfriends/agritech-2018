package com.agritech.patubig.user;

import lombok.Data;

/**
 * @author Michael Ryan A. Paclibar <michaelryanpaclibar@gmail.com>
 */
@Data
public class UserRequest {
  private String username;
  private String password;
  private String firstName;
  private String middleName;
  private String lastName;
  private String address;
  private String contactNumber;
}
