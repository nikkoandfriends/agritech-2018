package com.a80k.afor.agritech2018.core.retrofit.ws;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by Jan Paolo Regalado on 3/15/18.
 */

public interface ServiceApi {


    @POST
    Call<ResponseBody> postAsync(@Header("Authorization") String authorization, @Url String url, @Body Object object);

    @GET
    Call<ResponseBody> getAsync(@Header("Authorization") String authorization, @Url String url);

    @POST
    Call<ResponseBody> postStringBody(@Header("Authorization") String authorization, @Url String url, @Body RequestBody accountId);

    @POST
    Call<ResponseBody> postObjectBody(@Header("Authorization") String authorization, @Url String url, @Body Object request);

}
