package com.a80k.afor.agritech2018.activities.remote;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.a80k.afor.agritech2018.R;
import com.a80k.afor.agritech2018.activities.LoginActivity;
import com.a80k.afor.agritech2018.activities.product.ProductsActivity;
import com.a80k.afor.agritech2018.core.AppCallback;
import com.a80k.afor.agritech2018.core.AppPreference;
import com.a80k.afor.agritech2018.core.factory.AppFactory;
import com.a80k.afor.agritech2018.data.remote.data.ValveStatusResponse;
import com.a80k.afor.agritech2018.util.DialogHelper;

import java.util.Map;

/**
 * Created by Bautista on 9/15/2018.
 */

public class RemoteActivity extends AppCompatActivity {
    private Button manualButton;
    private boolean isWatering = false;
    private Button progressView;
    private RelativeLayout progressLayout;

    private Button scheduleButton;
    private TextView waterPercentageTv;
    private TextView waterInLitreTv;
    private int containerHeight;
    private LinearLayout waterLevelLayout;
    private double currentContainerLitre;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.remote_activity);
        manualButton = (Button) findViewById(R.id.toggle_btn);
        manualButton.setOnClickListener(onSetManualClickListener);
        progressView = (Button) findViewById(R.id.progress_view);
        progressLayout = (RelativeLayout) findViewById(R.id.progress_layout);
        scheduleButton = (Button) findViewById(R.id.schedule_btn);
        scheduleButton.setOnClickListener(getOnScheduleClicked());
        waterPercentageTv = (TextView) findViewById(R.id.water_level_tv);
        waterInLitreTv = (TextView) findViewById(R.id.water_litre_tv);
        waterLevelLayout = (LinearLayout) findViewById(R.id.water_level_layout);
        progressLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                progressLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                containerHeight = progressLayout.getHeight();
                Log.w("ContainerHeight", String.valueOf(containerHeight));

            }
        });

        findViewById(R.id.product_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RemoteActivity.this, ProductsActivity.class));
                overridePendingTransition(R.anim.slide_in, R.anim.slide_up);
//                startActivity(new Intent(RemoteActivity.this, HistoriesActivity.class));
            }
        });
        waterInLitreTv.setOnLongClickListener(getOnWaterLevelTapped());
        waterLevelLayout.setOnLongClickListener(getOnWaterLevelTapped());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);

    }

    private View.OnLongClickListener getOnWaterLevelTapped() {
        return new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                DialogHelper.displayGetContainerSizeDialog(RemoteActivity.this, new AppCallback() {
                    @Override
                    public void onCompleted(Object response, String message) {
                        if (message.isEmpty()) {
                            currentContainerLitre = (double) response;
                        }
                    }
                });
                return true;
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.exit: {
                clearPreferences();
                navigateToLogin();
                break;
            }
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void clearPreferences() {
        AppPreference.setName("");
        AppPreference.setUnionToken("");
        AppPreference.setUserEmail("");
        AppPreference.setLoggedIn(false);
        AppPreference.setRememberedPass("");
        AppPreference.setToken("");
        AppPreference.setUserId("");
    }

    private void navigateToLogin() {
        startActivity(new Intent(RemoteActivity.this, LoginActivity.class));
        finish();
    }

    @NonNull
    private View.OnClickListener getOnScheduleClicked() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (isWatering) {
//
//                    return;
//                }
//                DialogHelper.displaySetScheduleDialog(RemoteActivity.this, new AppCallback() {
//                    @Override
//                    public void onCompleted(Object response, String message) {
//                        toggleValveSchedule((Map<String, Object>) response);
//                        toggleSprinkleButton();
//                        toggleScheduleButton();
//                    }
//                });
                updateContainerValues();
            }
        };
    }

    private View.OnClickListener onSetManualClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            AppFactory.getRemoteLogic().normalToggle();
            toggleSprinkleButton();
//            if (isWatering) {
//
//                return;
//            }
//            DialogHelper.displaySetPercentageDialog(RemoteActivity.this, new AppCallback() {
//                @Override
//                public void onCompleted(Object response, String message) {
//                    toggleValveManual((Map<String, Object>) response);
//                    toggleSprinkleButton();
//                    toggleScheduleButton();
//                }
//            });
        }
    };

    private void toggleSprinkleButton() {
        isWatering = !isWatering;
        if (isWatering) {
            manualButton.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_shower_off), null,null,null);
            manualButton.setText("SWITCH VALVE: OFF");
//            manualButton.setAlpha(0.25f);
//            manualButton.setEnabled(false);
        } else {
            manualButton.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_shower_on), null,null,null);
            manualButton.setText("SWITCH VALVE: ON");
//            manualButton.setAlpha(1.0f);
//            manualButton.setEnabled(true);
        }
    }

    private void toggleScheduleButton() {
        if (isWatering) {
            scheduleButton.setAlpha(0.25f);
            scheduleButton.setEnabled(false);
        } else {
            scheduleButton.setAlpha(1.0f);
            scheduleButton.setEnabled(true);
//            scheduleButton.setText("SCHEDULE VALVE");
        }
    }

    private void setWaterLevel(int level) {
        waterPercentageTv.setText(level + "%");
        double percent = ((double) level / (double) 100) * (double) containerHeight;
        Log.w("Percent", String.valueOf(percent));
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(progressView.getWidth(), (int) percent);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        progressView.setLayoutParams(params);
    }

    private void setContainerData(int currentLevel) {
        double percentage = ((double)currentLevel/(double)100)*currentContainerLitre;
        waterInLitreTv.setText(String.format("%.2f", percentage) + " litre(s)/ "+ currentContainerLitre + " litre(s)");
    }

    private void updateContainerValues() {
        Toast.makeText(RemoteActivity.this, "Retrieving container data.", Toast.LENGTH_SHORT).show();
        AppFactory.getRemoteLogic().pullContainerData(new AppCallback() {
            @Override
            public void onCompleted(Object response, String message) {
                if (message.isEmpty()) {
                    ValveStatusResponse response1 = (ValveStatusResponse) response;
                    setWaterLevel(response1.getWaterLevel());
                    setContainerData(response1.getWaterLevel());
                } else {
                    Toast.makeText(RemoteActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void toggleValveSchedule(Map<String, Object> schedule) {
        AppFactory.getRemoteLogic().toggleValveSchedule(schedule, new AppCallback() {
            @Override
            public void onCompleted(Object response, String message) {
                if (message.isEmpty()) {
                    updateContainerValues();
                } else {
                    toggleSprinkleButton();
                    toggleScheduleButton();
                    Toast.makeText(RemoteActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void toggleValveManual(Map<String, Object> percent) {
        AppFactory.getRemoteLogic().toggleValveManual(percent, new AppCallback() {
            @Override
            public void onCompleted(Object response, String message) {
//                if (message.isEmpty()) {
//                    updateContainerValues();
//                } else {
                    toggleSprinkleButton();
                    toggleScheduleButton();
                    Toast.makeText(RemoteActivity.this, message, Toast.LENGTH_SHORT).show();
//                }
            }
        });
    }

    private void cancelValveProcess() {
        AppFactory.getRemoteLogic().cancelValveProcess(new AppCallback() {
            @Override
            public void onCompleted(Object response, String message) {
                if (message.isEmpty()) {
                    Toast.makeText(RemoteActivity.this, "Successfully cancelled valve process.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(RemoteActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
