package com.a80k.afor.agritech2018.activities.product;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.a80k.afor.agritech2018.R;
import com.a80k.afor.agritech2018.data.product.data.History;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Bautista on 9/16/2018.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryHolder> {
    private List<History> historyList = new ArrayList<>();
    private View.OnClickListener onClickListener;
    private Context context;

    public HistoryAdapter(List<History> historyList, Context context, View.OnClickListener onClickListener) {
        this.historyList = historyList;
        this.onClickListener = onClickListener;
        this.context = context;
    }


    @NonNull
    @Override
    public HistoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_row_layout, parent, false);
        HistoryHolder adapter = new HistoryHolder(layout);
        return adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryHolder holder, int position) {
        History history = historyList.get(position);
        holder.product_price.setText(String.format(context.getString(R.string.price_format), history.getItem().getPrice()));
        holder.product_title.setText(history.getItem().getName());
        holder.date_created.setText(toDate(history.getCreatedDate()));
        holder.product_quantity.setText("Qty: " + history.getQuantity());

        holder.layout.setTag(history);
        holder.layout.setOnClickListener(onClickListener);
    }

    private String toDate(long time) {
        String pattern = "MMMM dd, yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date(time));
        return date;
    }

    @Override
    public int getItemCount() {
        if(historyList != null){
            return historyList.size();
        }
        return 0;
    }

    public class HistoryHolder extends RecyclerView.ViewHolder {

        private TextView product_title;
        private TextView product_price;
        private LinearLayout layout;
        private TextView product_quantity;
        private TextView date_created;
        public HistoryHolder(View itemView) {
            super(itemView);


            layout = itemView.findViewById(R.id.product_ll);
            date_created = itemView.findViewById(R.id.date_created);
            product_title = itemView.findViewById(R.id.product_title);
            product_price = itemView.findViewById(R.id.product_price);
            product_quantity = itemView.findViewById(R.id.quantity_tv);
        }
    }
}
