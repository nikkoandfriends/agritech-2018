package com.a80k.afor.agritech2018.activities.unionbank;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.a80k.afor.agritech2018.BuildConfig;
import com.a80k.afor.agritech2018.R;
import com.a80k.afor.agritech2018.core.AppCallback;
import com.a80k.afor.agritech2018.core.AppPreference;
import com.google.gson.Gson;

public class UnionLoginPage extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.union_login_layout);

        WebView myWebView = findViewById(R.id.web_view);
        WebSettings settings = myWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowContentAccess(true);
        settings.setDomStorageEnabled(true);
        myWebView.setWebViewClient(new MyClient(new AppCallback() {
            @Override
            public void onCompleted(Object response, String message) {
                if(!message.isEmpty()){
                    Toast.makeText(UnionLoginPage.this,message, Toast.LENGTH_SHORT).show();
                }
                finish();
            }
        }));
        String url = BuildConfig.UNION_URL.concat("api/login");
        myWebView.loadUrl(url);

    }
}

class MyClient extends WebViewClient {

    public MyClient(AppCallback callback) {
        this.callback = callback;
    }

    private AppCallback callback;
    private int count = 0;

    @Override
    public void onPageFinished(WebView view, final String url) {
        Log.w("FINISH", url);
        view.evaluateJavascript(
                "(function() { return ('<html>'+document.getElementsByTagName('body')[0].innerHTML+'</html>'); })();",
                new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String html) {
                        String loginUrl = BuildConfig.UNION_URL.concat("api/login");
                        boolean firstUrl = url.equals(loginUrl);
                        cutAndParseHtml(html, firstUrl, url);
                    }
                });
        super.onPageFinished(view, url);
    }

    private void cutAndParseHtml(String html, boolean firstUrl, String url) {
        if(url.startsWith(BuildConfig.UNION_URL) && !firstUrl){
            count++;
        }
        Log.w("COUNT", String.valueOf(count));
        if (count == 2) {
            try {
                String s1 = html.substring(html.indexOf("{"));
                String result = s1.substring(0, s1.indexOf("}") + 1);
                Log.w("FINAL STRING", result);
                String json = result.replaceAll("\\\\", "");

                Gson gson = new Gson();
                UnionToken token = gson.fromJson(json, UnionToken.class);
                AppPreference.setUnionToken(token.getAccess_token());
                callback.onCompleted(null, "");
            }catch (Exception e){
                callback.onCompleted(null, "Something went wrong");
            }
        }
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, final String url) {
        Log.w("GET_URL", url);
        if (url.startsWith("https://gumanakaplease.localtunnel")) {
            return false;

        } else {

        }
        view.loadUrl(url);
        return true;
    }
}
