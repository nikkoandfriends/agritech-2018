package com.a80k.afor.agritech2018.core.auth;

import com.a80k.afor.agritech2018.core.AppCallback;

public interface AuthLogic {
    String GET_TOKEN_POSTFIX = "/api/auth";

    void getToken(AppCallback callback);
}
