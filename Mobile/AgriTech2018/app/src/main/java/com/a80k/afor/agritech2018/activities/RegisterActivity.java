package com.a80k.afor.agritech2018.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.a80k.afor.agritech2018.R;
import com.a80k.afor.agritech2018.core.AppCallback;
import com.a80k.afor.agritech2018.core.factory.AppFactory;
import com.a80k.afor.agritech2018.user.User;

public class RegisterActivity extends AppCompatActivity {
    private EditText fnameEt, lnameEt, emailEt, contactEt, passwordEt, confirmPassEt;
    private AlertDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_layout);
        fnameEt = findViewById(R.id.fname);
        lnameEt = findViewById(R.id.lname);
        emailEt = findViewById(R.id.email);
        contactEt = findViewById(R.id.contact_number);
        passwordEt = findViewById(R.id.password);
        confirmPassEt = findViewById(R.id.confirm_password);


        registerClickListener();
        enableBackButton();
    }

    private void registerClickListener() {
        findViewById(R.id.register_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = new User();
                String pass = passwordEt.getText().toString().trim();
                String confirmPass = confirmPassEt.getText().toString().trim();
                if(pass.isEmpty()){
                    Toast.makeText(RegisterActivity.this, "Password is required", Toast.LENGTH_SHORT).show();
                    return;
                }
                verifyUser(user, pass, confirmPass);
            }
        });
    }

    public void showProgress() {
        if (dialog == null) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(RegisterActivity.this);
            View view = getLayoutInflater().inflate(R.layout.progress_dialog, null);
            TextView textView = view.findViewById(R.id.message);
            textView.setText("Creating User...");
            dialogBuilder.setView(view);
            dialogBuilder.setCancelable(false);
            dialog = dialogBuilder.create();
        }
        dialog.show();
    }

    public void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private void verifyUser(User user, String pass, String confirmPass) {
        if(pass.equals(confirmPass)) {
            user.setContactNumber(contactEt.getText().toString());
            user.setLastName(lnameEt.getText().toString());
            user.setFirstName(fnameEt.getText().toString());
            user.setEmail(emailEt.getText().toString());
            user.setPassword(pass);
            showProgress();
            AppFactory.getUserLogicInstance().register(user, new AppCallback() {
                @Override
                public void onCompleted(Object response, String message) {
                    if(RegisterActivity.this != null) {
                        hideDialog();
                        if (message.isEmpty()) {
                            Toast.makeText(RegisterActivity.this, "Registration Successful", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }else {
            Toast.makeText(RegisterActivity.this, "Passwords does not match", Toast.LENGTH_SHORT).show();
        }
    }

    private void enableBackButton() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
