package com.a80k.afor.agritech2018.user;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;


public class User implements Parcelable {

    private String contactNumber;
    private String username;
    private String firstName;
    private String id;
    private String lastName;
    private String password;
    private String userName;
    private List<String> roles;

    public User() {
    }


    protected User(Parcel in) {
        contactNumber = in.readString();
        username = in.readString();
        firstName = in.readString();
        id = in.readString();
        lastName = in.readString();
        password = in.readString();
        userName = in.readString();
        roles = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(contactNumber);
        dest.writeString(username);
        dest.writeString(firstName);
        dest.writeString(id);
        dest.writeString(lastName);
        dest.writeString(password);
        dest.writeString(userName);
        dest.writeStringList(roles);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmail() {
        return username;
    }

    public void setEmail(String email) {
        this.username = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
