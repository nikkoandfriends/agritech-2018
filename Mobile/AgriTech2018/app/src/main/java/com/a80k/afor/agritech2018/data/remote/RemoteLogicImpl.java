package com.a80k.afor.agritech2018.data.remote;

import com.a80k.afor.agritech2018.core.AppCallback;
import com.a80k.afor.agritech2018.core.auth.AuthLogicImpl;
import com.a80k.afor.agritech2018.core.retrofit.ws.Service;
import com.a80k.afor.agritech2018.data.remote.data.ValveStatusResponse;

import java.util.Map;

/**
 * Created by Bautista on 9/15/2018.
 */

public class RemoteLogicImpl extends AuthLogicImpl implements RemoteLogic {
    private static RemoteLogic INSTANCE;

    public RemoteLogicImpl(Service mApi) {
        super(mApi);
    }

    public static RemoteLogic getInstance(Service service){
        if(INSTANCE == null){
            INSTANCE = new RemoteLogicImpl(service);
        }
        return INSTANCE;
    }

    @Override
    public void toggleValveManual(final Map<String, Object> percent, final AppCallback callback) {
        getToken(new AppCallback() {
            @Override
            public void onCompleted(Object response, String message) {
                if(message.isEmpty()){
                    String token = (String) response;
                    mApi.postAsync(TOGGLE_VALVE, token, percent, Object.class, callback);
                }else {
                    callback.onCompleted(response, message);
                }
            }
        });
    }

    @Override
    public void toggleValveSchedule(final Map<String, Object> schedule, final AppCallback callback) {
        getToken(new AppCallback() {
            @Override
            public void onCompleted(Object response, String message) {
                if(message.isEmpty()){
                    String token = (String) response;
                    mApi.postAsync(TOGGLE_VALVE_SCHEDULE, token, schedule, Object.class, callback);
                }else {
                    callback.onCompleted(response, message);
                }
            }
        });
    }

    @Override
    public void pullContainerData(final AppCallback callback) {
      getToken(new AppCallback() {
          @Override
          public void onCompleted(Object response, String message) {
              if (message.isEmpty()) {
                  String token = (String)response;
                  mApi.getAsync(GET_COTAINER_DATA, token, ValveStatusResponse.class, callback);
              } else {
                  callback.onCompleted(null, message);
              }
          }
      });
    }

    @Override
    public void cancelValveProcess(final AppCallback callback) {
        getToken(new AppCallback() {
            @Override
            public void onCompleted(Object response, String message) {
                if (message.isEmpty()) {
                    String token = (String)response;
                    mApi.postAsync(CANCEL_VALVE_PROCESS, token, null, Object.class, callback);
                } else {
                    callback.onCompleted(null, message);
                }
            }
        });
    }

    @Override
    public void normalToggle() {
        getToken(new AppCallback() {
            @Override
            public void onCompleted(Object response, String message) {
                if (message.isEmpty()) {
                    String token = (String)response;
                    mApi.getAsync(TOGGLE_NORMAL, token, Object.class, new AppCallback() {
                        @Override
                        public void onCompleted(Object response, String message) {

                        }
                    });
                } else {
//                    callback.onCompleted(null, message);
                }
            }
        });
    }
}
