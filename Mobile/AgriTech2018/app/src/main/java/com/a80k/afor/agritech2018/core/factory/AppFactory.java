package com.a80k.afor.agritech2018.core.factory;

import com.a80k.afor.agritech2018.core.retrofit.ws.RestUrlHelper;
import com.a80k.afor.agritech2018.core.retrofit.ws.ServiceApi;
import com.a80k.afor.agritech2018.core.retrofit.ws.ServiceApiImpl;
import com.a80k.afor.agritech2018.data.product.logic.ProductLogic;
import com.a80k.afor.agritech2018.data.product.logic.ProductLogicImpl;
import com.a80k.afor.agritech2018.user.UserLogic;
import com.a80k.afor.agritech2018.user.UserLogicImpl;
import com.a80k.afor.agritech2018.data.remote.RemoteLogic;
import com.a80k.afor.agritech2018.data.remote.RemoteLogicImpl;

public class AppFactory {
    public static ProductLogic getProducLogic() {
        return ProductLogicImpl.getInstance(new ServiceApiImpl
                (RestUrlHelper.getInstance().createAppApiInstance(ServiceApi.class)));
    }

    public static RemoteLogic getRemoteLogic() {
        return RemoteLogicImpl.getInstance(new ServiceApiImpl(RestUrlHelper.getInstance().createAppApiInstance(ServiceApi.class)));
    }

    public static UserLogic getUserLogicInstance() {
        return UserLogicImpl.getINSTANCE(new ServiceApiImpl
                (RestUrlHelper.getInstance().createAppApiInstance(ServiceApi.class)));
    }
}
