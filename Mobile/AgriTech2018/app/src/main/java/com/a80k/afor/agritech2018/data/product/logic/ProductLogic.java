package com.a80k.afor.agritech2018.data.product.logic;

import com.a80k.afor.agritech2018.core.AppCallback;
import com.a80k.afor.agritech2018.data.product.data.ProductRequest;

public interface ProductLogic {

    String GET_PRODUCTS = "/api/item/list";

    String BUY_ITEM = "/api/item/paymerchant";

    String GET_HISTORY = "/api/item/itemtransactions";

    void downloadProducts(AppCallback callback);

    void downloadHistories(AppCallback callback);

    void buyItem(ProductRequest request, AppCallback callback);
}
