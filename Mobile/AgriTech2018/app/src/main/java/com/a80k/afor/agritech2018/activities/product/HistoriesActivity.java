package com.a80k.afor.agritech2018.activities.product;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.a80k.afor.agritech2018.R;
import com.a80k.afor.agritech2018.core.AppCallback;
import com.a80k.afor.agritech2018.core.factory.AppFactory;
import com.a80k.afor.agritech2018.data.product.data.History;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Bautista on 9/16/2018.
 */

public class HistoriesActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    private RecyclerView recyclerView;
    private List<History> historyList;
    private SwipeRefreshLayout swipeLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.histories_activity);
        swipeLayout = findViewById(R.id.swipe);
        swipeLayout.setOnRefreshListener(this);
        initRecycler();
//        historyList = MockProduct.getProductList();
        initRecycler();
        initRecyclerView();

        enableBackButton();

        downloadProducts();

    }

    private void downloadProducts() {
        swipeLayout.setRefreshing(true);
        AppFactory.getProducLogic().downloadHistories(new AppCallback() {
            @Override
            public void onCompleted(Object response, String message) {
                if (HistoriesActivity.this != null) {
                    swipeLayout.setRefreshing(false);
                    if (message.isEmpty()) {
                        History[] products = (History[]) response;
                        historyList = Arrays.asList(products);
                        initRecyclerView();
                    } else
                        Toast.makeText(HistoriesActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void enableBackButton() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    private void initRecyclerView() {
        HistoryAdapter adapter = new HistoryAdapter(historyList, HistoriesActivity.this, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                History history = (History) view.getTag();
//                Intent intent = new Intent(HistoriesActivity.this, ProductActivity.class);
//                intent.putExtra(ProductActivity.PRODUCT_EXTRA, history);
//                startActivity(intent);
            }
        });

        recyclerView.setAdapter(adapter);
    }

    private void initRecycler() {
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
    }

    @Override
    public void onRefresh() {
        downloadProducts();
    }
}
