package com.a80k.afor.agritech2018.util;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.a80k.afor.agritech2018.R;
import com.a80k.afor.agritech2018.core.AppCallback;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Bautista on 9/15/2018.
 */

public class DialogHelper {
    public static void displaySetScheduleDialog(final Activity activity, final AppCallback callback) {
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.set_schedule_layout);
        final Spinner spinner = dialog.findViewById(R.id.interval_spinner);
        List<String> dropDownValues = Arrays.asList("Hour(s)", "Minute(s)");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_dropdown_item, dropDownValues);
        spinner.setAdapter(arrayAdapter);
        final Map<String,Integer> schedule = new HashMap<>();
        schedule.put("waterToConsume", 0);
        schedule.put("timeStartHour", 0);
        schedule.put("timeStartMin", 0);
        schedule.put("intervalInMin", 0);
        final TextView timeTextView = dialog.findViewById(R.id.time_tv);
        timeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(activity, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                        Log.w("OnTimePick", "Hour of day: " + hour + " minute: " + minute);
                        String hourValue = String.valueOf(hour);
                        String minuteValue = String.valueOf(minute);
                        String timeIndicator = "AM";
                        if (hour >= 12) {
                            timeIndicator = "PM";
                        }
                        if (hour < 10) {
                            hourValue = "0" + hour;
                        } else if ((hour-12) < 10) {
                            hourValue = "0" + (hour-12);
                        }
                        if (minute < 10) {
                            minuteValue = "0" + minute;
                        }
                        timeTextView.setText(hourValue + ":" + minuteValue + " " + timeIndicator);
                        schedule.put("timeStartHour", hour);
                        schedule.put("timeStartMin", minute);
                    }
                },0,0,false);
                timePickerDialog.show();
            }
        });
        final EditText intervalEt = dialog.findViewById(R.id.interval_et);
        final EditText percent = dialog.findViewById(R.id.percent_per_interval_et);
        dialog.findViewById(R.id.cancel_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.set_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (hasValues(intervalEt,percent)) {
                    int interval = Integer.parseInt(intervalEt.getText().toString());

                    int percentValue = Integer.parseInt(percent.getText().toString());
                    String timeIndicator = spinner.getSelectedItem().toString();

                    if (percentValue > 100) {
                        Toast.makeText(activity, "Cannot assign percent value above 100.", Toast.LENGTH_SHORT).show();
                        return;
                    } else if (timeTextView.getText().toString().equals("Tap to Set time")) {
                        Toast.makeText(activity, "Please set the time to start.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    schedule.put("waterToConsume", percentValue);
                    int intervalInMinutes = 0;
                    if (timeIndicator.equals("Hour(s)")) {
                        intervalInMinutes *= 60;
                    } else {
                        intervalInMinutes = interval;
                    }
                    schedule.put("intervalInMin",  intervalInMinutes);
                    Log.w("Schedule", schedule.toString());

                    callback.onCompleted(schedule, "");
                    dialog.dismiss();
                } else {
                    Toast.makeText(activity, "Please provide entries in order to proceed", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dialog.show();
    }


    public static void displaySetPercentageDialog(final Activity activity, final AppCallback callback) {
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.set_percentage_layout);
        final Map<String,Integer> schedule = new HashMap<>();
        schedule.put("waterToConsume", 0);
        final EditText percent = dialog.findViewById(R.id.percent_per_interval_et);
        dialog.findViewById(R.id.cancel_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.set_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (hasValues(percent)) {
                    int percentValue = Integer.parseInt(percent.getText().toString());

                    if (percentValue > 100) {
                        Toast.makeText(activity, "Cannot assign percent value above 100", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    schedule.put("waterToConsume", percentValue);
                    callback.onCompleted(schedule, "");
                    dialog.dismiss();
                } else {
                    Toast.makeText(activity, "Please provide entries in order to proceed", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dialog.show();
    }
    private static boolean hasValues(TextView...textViews) {
        for (TextView textView : textViews) {
            if (textView.getText().toString().isEmpty()) {
                return false;
            }
        }
        return true;
    }

    public static void displayGetContainerSizeDialog(final Activity activity, final AppCallback callback) {
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.water_container_edit_layout);
        final EditText litre_entry = dialog.findViewById(R.id.litre_et);
        dialog.findViewById(R.id.cancel_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.set_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hasValues(litre_entry)) {
                    String entry = litre_entry.getText().toString();
                    try {
                        double entryValue = Double.parseDouble(entry);
                        callback.onCompleted(entryValue, "");
                        dialog.dismiss();
                    } catch (NumberFormatException e) {
                        Toast.makeText(activity, "Please enter valid data.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(activity, "Please enter your container size.", Toast.LENGTH_SHORT).show();
                }


            }
        });
        dialog.show();
    }
}
