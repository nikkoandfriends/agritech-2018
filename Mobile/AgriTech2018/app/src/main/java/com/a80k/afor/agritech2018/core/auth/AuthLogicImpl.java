package com.a80k.afor.agritech2018.core.auth;

import com.a80k.afor.agritech2018.core.AppCallback;
import com.a80k.afor.agritech2018.core.AppPreference;
import com.a80k.afor.agritech2018.core.retrofit.ws.Service;
import com.a80k.afor.agritech2018.user.User;
import com.auth0.android.jwt.JWT;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.Base64Utils;

import java.util.HashMap;
import java.util.Map;

public class AuthLogicImpl implements AuthLogic{

    protected Service mApi;

    public AuthLogicImpl(Service mApi) {
        this.mApi = mApi;
    }

    @Override
    public void getToken(AppCallback callback) {
        if (AppPreference.getToken().isEmpty() || AppPreference.getTokenAge() < System.currentTimeMillis()) {
            authenticate(AppPreference.getRememberedUName(), AppPreference.getRememberedPass(), callback);
        } else {
            callback.onCompleted(AppPreference.getToken(), "");
        }
    }
    private void authenticate(String username, String password, final AppCallback callback) {
        Map<String, String> request = new HashMap<>();
        request.put("username", username);
        request.put("password", password);
        mApi.postAsync(AuthLogic.GET_TOKEN_POSTFIX, null, request, TokenHolder.class, new AppCallback() {
            @Override
            public void onCompleted(Object response, String message) {
                if (message.isEmpty()) {
                    TokenHolder holder = (TokenHolder) response;
                    String[] split_string = holder.getToken().split("\\.");
                    String base64EncodedBody = split_string[1];

                    JWT jwt = new JWT(holder.getToken());
                    String body = new String((Base64Utils.decode(base64EncodedBody)));

                    try {
                        JSONObject obj = new JSONObject(body);
                        String object = obj.getJSONObject("user").toString();
                        saveUserToPref(object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    long expiration = jwt.getExpiresAt().getTime();
                    setToken(holder.getToken(), expiration);
                    callback.onCompleted("","");
                } else {
                    callback.onCompleted(null, message);
                }

            }
        });
    }

    private void saveUserToPref(String object) {
        User user = new Gson().fromJson(object, User.class);
        AppPreference.setUserEmail(user.getEmail());
        AppPreference.setUserId(user.getId());
        AppPreference.setName(user.getFirstName() + " " +user.getLastName());
    }

    private void setToken(String token, long expirationTime) {
        AppPreference.setToken(token);
        long tokenAge = System.currentTimeMillis() + (expirationTime * 1000);
        AppPreference.setTokenAge(expirationTime - 60000);
    }

}
