package com.a80k.afor.agritech2018.data.remote.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bautista on 9/15/2018.
 */

public class ValveStatusResponse implements Parcelable{
    private int waterLevel;
    private int valveStatus;


    protected ValveStatusResponse(Parcel in) {
        waterLevel = in.readInt();
        valveStatus = in.readInt();
    }

    public static final Creator<ValveStatusResponse> CREATOR = new Creator<ValveStatusResponse>() {
        @Override
        public ValveStatusResponse createFromParcel(Parcel in) {
            return new ValveStatusResponse(in);
        }

        @Override
        public ValveStatusResponse[] newArray(int size) {
            return new ValveStatusResponse[size];
        }
    };

    public void setValveStatus(int valveStatus) {
        this.valveStatus = valveStatus;
    }

    public int getValveStatus() {
        return valveStatus;
    }

    public void setWaterLevel(int waterLevel) {
        this.waterLevel = waterLevel;
    }

    public int getWaterLevel() {
        return waterLevel;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(waterLevel);
        dest.writeInt(valveStatus);
    }
}
