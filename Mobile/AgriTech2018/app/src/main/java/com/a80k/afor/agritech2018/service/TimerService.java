package com.a80k.afor.agritech2018.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

public class TimerService extends Service {
    public static final String ACTION_TIMER_SERVICE = "com.a80k.afor.agritech2018.service.TimerService";


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return START_STICKY;
    }


}
