package com.a80k.afor.agritech2018.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.a80k.afor.agritech2018.R;
import com.a80k.afor.agritech2018.activities.remote.RemoteActivity;
import com.a80k.afor.agritech2018.core.AppCallback;
import com.a80k.afor.agritech2018.core.AppPreference;
import com.a80k.afor.agritech2018.core.auth.AuthLogicImpl;
import com.a80k.afor.agritech2018.core.retrofit.ws.RestUrlHelper;
import com.a80k.afor.agritech2018.core.retrofit.ws.ServiceApi;
import com.a80k.afor.agritech2018.core.retrofit.ws.ServiceApiImpl;

public class LoginActivity extends AppCompatActivity {

    private EditText emailEt, passwordEt;
    private AlertDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        emailEt = findViewById(R.id.email);
        passwordEt = findViewById(R.id.password);
        if (AppPreference.isLoggedIn()) {
            navigateAndDismiss();
        }

        findViewById(R.id.login_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerActivity();
            }
        });

    }

    private void registerActivity() {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
    }

    public void showProgress() {
        if (dialog == null) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LoginActivity.this);
            View view = getLayoutInflater().inflate(R.layout.progress_dialog, null);
            TextView textView = view.findViewById(R.id.message);
            textView.setText("Retrieving User...");
            dialogBuilder.setView(view);
            dialogBuilder.setCancelable(false);
            dialog = dialogBuilder.create();
        }
        dialog.show();
    }

    public void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }


    private void login() {
        String pass = passwordEt.getText().toString().trim();
        String email = emailEt.getText().toString().trim();

        AppPreference.setRememberedUname(email);
        AppPreference.setRememberedPass(pass);
        showProgress();
        new AuthLogicImpl(new ServiceApiImpl(RestUrlHelper.getInstance().
                createAppApiInstance(ServiceApi.class))).getToken(new AppCallback() {
            @Override
            public void onCompleted(Object response, String message) {
                hideDialog();
                if (message.isEmpty()) {
                    AppPreference.setLoggedIn(true);
                    navigateAndDismiss();
                } else {
                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void navigateAndDismiss() {
        startActivity(new Intent(LoginActivity.this, RemoteActivity.class));
        finish();
    }
}
