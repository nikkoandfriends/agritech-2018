package com.a80k.afor.agritech2018.activities.unionbank;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface ClientService {
    @GET
    Call<UnionToken> getClientList(@Url String url);
}
