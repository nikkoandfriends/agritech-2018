package com.a80k.afor.agritech2018.activities.product;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.a80k.afor.agritech2018.R;
import com.a80k.afor.agritech2018.data.product.data.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductHolder> {

    private List<Product> productList = new ArrayList<>();
    private View.OnClickListener onClickListener;
    private Context context;

    public ProductAdapter(List<Product> productList, Context context, View.OnClickListener onClickListener) {
        this.productList = productList;
        this.onClickListener = onClickListener;
        this.context = context;
    }

    @NonNull
    @Override
    public ProductHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_row_layout, parent, false);
        ProductHolder adapter = new ProductHolder(layout);
        return adapter;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductHolder holder, int position) {
        Product product = productList.get(position);
        holder.product_price.setText(String.format(context.getString(R.string.price_format), product.getPrice()));
        holder.product_title.setText(product.getName());
        holder.layout.setTag(product);
        holder.layout.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        if(productList != null){
            return productList.size();
        }
        return 0;
    }

    public class ProductHolder extends RecyclerView.ViewHolder {

        private ImageView product_image;
        private TextView product_title;
        private TextView product_price;
        private LinearLayout layout;


        public ProductHolder(View itemView) {
            super(itemView);

            layout = itemView.findViewById(R.id.product_ll);
            product_image = itemView.findViewById(R.id.product_image);
            product_title = itemView.findViewById(R.id.product_title);
            product_price = itemView.findViewById(R.id.product_price);
        }
    }
}
