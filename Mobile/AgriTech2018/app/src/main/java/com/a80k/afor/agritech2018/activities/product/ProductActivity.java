package com.a80k.afor.agritech2018.activities.product;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.a80k.afor.agritech2018.R;
import com.a80k.afor.agritech2018.activities.unionbank.UnionLoginPage;
import com.a80k.afor.agritech2018.core.AppCallback;
import com.a80k.afor.agritech2018.core.AppPreference;
import com.a80k.afor.agritech2018.core.factory.AppFactory;
import com.a80k.afor.agritech2018.data.product.data.Product;
import com.a80k.afor.agritech2018.data.product.data.ProductRequest;
import com.a80k.afor.agritech2018.data.product.data.ProductType;

public class ProductActivity extends AppCompatActivity {

    public static final String PRODUCT_EXTRA = "com.a80k.afor.agritech2018.activities.product.PRODUCT_EXTRA";
    private int quantity;
    private Product product;
    private AlertDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_main_view);
        product = getIntent().getParcelableExtra(PRODUCT_EXTRA);

        initViews();
        enableBackButton();
    }

    private void enableBackButton() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initViews() {
        ImageView image = findViewById(R.id.image);

        if (product.getType().equals(ProductType.FERTILIZER)) {
            image.setImageDrawable(getResources().getDrawable(R.drawable.ic_fertilizer));
        } else
            image.setImageDrawable(getResources().getDrawable(R.drawable.ic_pesticide));


        TextView title = findViewById(R.id.item_name);
        TextView description = findViewById(R.id.item_desc);
        TextView price = findViewById(R.id.item_price);

        title.setText(product.getName());
        description.setText(product.getDescription());
        price.setText(String.format(getString(R.string.price_format), product.getPrice()));

        findViewById(R.id.buy_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(ProductActivity.this);
                View dialogView = getLayoutInflater().inflate(R.layout.quantity_dialog_layout, null);
                final EditText qty = dialogView.findViewById(R.id.quantity);
                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        quantity = Integer.parseInt(qty.getText().toString().trim());
                        if (quantity == 0) {
                            Toast.makeText(ProductActivity.this, "Invalid quantity", Toast.LENGTH_SHORT).show();
                        } else {
                            startActivity(new Intent(ProductActivity.this, UnionLoginPage.class));
                        }
                    }
                });
                dialog.setTitle("Enter item quantity");
                dialog.setNegativeButton("Cancel", null);
                dialog.setView(dialogView);
                dialog.show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        AppPreference.setUnionToken("");
        super.onDestroy();
    }

    public void showProgress() {
        if (dialog == null) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ProductActivity.this);
            View view = getLayoutInflater().inflate(R.layout.progress_dialog, null);
            TextView textView = view.findViewById(R.id.message);
            textView.setText("Preparing transaction...");
            dialogBuilder.setView(view);
            dialogBuilder.setCancelable(false);
            dialog = dialogBuilder.create();
        }
        dialog.show();
    }

    public void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!AppPreference.getUnionToken().isEmpty()) {
            ProductRequest request = new ProductRequest();
            request.setAccessToken(AppPreference.getUnionToken());
            request.setFullName(AppPreference.getName());
            request.setItem(product);
            request.setQuantity(quantity);
            request.setUserId(AppPreference.getUserId());
            showProgress();

            AppFactory.getProducLogic().buyItem(request, new AppCallback() {
                @Override
                public void onCompleted(Object response, final String message) {
                    if (ProductActivity.this != null) {
                        handleResponse(message);
                    }
                }
            });
        }
    }

    private void handleResponse(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideDialog();
                if (message.isEmpty()) {
                    Toast.makeText(ProductActivity.this, "Transaction successful!", Toast.LENGTH_SHORT).show();
                } else {
                    quantity = 0;
                    Toast.makeText(ProductActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
