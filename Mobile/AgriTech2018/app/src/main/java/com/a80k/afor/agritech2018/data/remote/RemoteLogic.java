package com.a80k.afor.agritech2018.data.remote;

import com.a80k.afor.agritech2018.core.AppCallback;

import java.util.Map;

/**
 * Created by Bautista on 9/15/2018.
 */

public interface RemoteLogic {
    String TOGGLE_VALVE = "/api/hardware/manualtoggle";
    String GET_COTAINER_DATA = "/api/hardware/status";
    String TOGGLE_VALVE_SCHEDULE = "/api/hardware/scheduledtoggle";
    String CANCEL_VALVE_PROCESS = "/api/hardware/cancel";
    String TOGGLE_NORMAL = "/api/hardware/valve/toggle";
    void toggleValveManual(Map<String, Object> percent, AppCallback callback);
    void toggleValveSchedule(Map<String, Object> schedule, AppCallback callback);
    void pullContainerData(AppCallback callback);
    void cancelValveProcess(AppCallback callback);
    void normalToggle();
}
