package com.a80k.afor.agritech2018.data.product.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bautista on 9/16/2018.
 */

public class History implements Parcelable{
    private String fullName;
    private String id;
    private Product item;
    private int quantity;
    private UnionBankTransfer unionBankTransfer;
    private String userId;
    private long createdDate;

    protected History(Parcel in) {
        fullName = in.readString();
        id = in.readString();
        item = in.readParcelable(Product.class.getClassLoader());
        quantity = in.readInt();
        unionBankTransfer = in.readParcelable(UnionBankTransfer.class.getClassLoader());
        userId = in.readString();
        createdDate = in.readLong();
    }

    public static final Creator<History> CREATOR = new Creator<History>() {
        @Override
        public History createFromParcel(Parcel in) {
            return new History(in);
        }

        @Override
        public History[] newArray(int size) {
            return new History[size];
        }
    };

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Product getItem() {
        return item;
    }

    public void setItem(Product item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public UnionBankTransfer getUnionBankTransfer() {
        return unionBankTransfer;
    }

    public void setUnionBankTransfer(UnionBankTransfer unionBankTransfer) {
        this.unionBankTransfer = unionBankTransfer;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fullName);
        dest.writeString(id);
        dest.writeParcelable(item, flags);
        dest.writeInt(quantity);
        dest.writeParcelable(unionBankTransfer, flags);
        dest.writeString(userId);
        dest.writeLong(createdDate);
    }
}
