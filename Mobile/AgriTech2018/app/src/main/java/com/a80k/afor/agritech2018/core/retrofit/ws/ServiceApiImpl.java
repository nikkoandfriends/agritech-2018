package com.a80k.afor.agritech2018.core.retrofit.ws;

import android.util.Log;

import com.a80k.afor.agritech2018.core.AppCallback;
import com.a80k.afor.agritech2018.core.retrofit.ws.error.ApiError;
import com.a80k.afor.agritech2018.core.retrofit.ws.error.ServiceErrorUtil;
import com.google.gson.Gson;

import org.springframework.web.client.HttpStatusCodeException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jan Paolo Regalado on 3/15/18.
 */

public class ServiceApiImpl implements Service {

    private ServiceApi mApi;

    public static final String GENERIC_ERROR = "Something went wrong. Please try again.";
    public static final String SERVER_FAILED = "Failed to connect to server. Please try again.";
    public static final String NETWORK_ERROR = "No Network Connection";


    public ServiceApiImpl(ServiceApi api) {
        this.mApi = api;
    }

    @Override
    public void getAsync(String url, String token, Class response, AppCallback callback) {
        Log.w("TOKEN", appendBearer(token));
        mApi.getAsync(appendBearer(token), url).enqueue(getResponse(response, callback));
    }

    @Override
    public void postAsync(String url, String token, Object request, Class response, AppCallback callback) {
        mApi.postAsync(appendBearer(token), url, request).enqueue(getResponse(response, callback));
    }

    @Override
    public Object postStringRequest(String url, String token, String request, Class responseClass) throws Exception {
        Log.w("TOKEN", appendBearer(token));
        Response<ResponseBody> bodyResponse = null;
        try {
            RequestBody requestBody = RequestBody.create(MediaType.parse("text/plain"), request);
            bodyResponse = mApi.postStringBody(appendBearer(token), url, requestBody).execute();
        } catch (Exception ex) {
            return throwException(ex);
        }
        Log.w("token", token);
        Log.w("REPONSE", String.format("ResponseCode: %s", bodyResponse.code()));
        Log.w("REPONSE", String.format("Response: %s", bodyResponse.message()));
        Log.w("REPONSE", bodyResponse.raw() == null ? "Raw response is null" : bodyResponse.raw().request().url().toString());
        return checkResponse(responseClass, bodyResponse);
    }

    @Override
    public Object postObjectRequest(String url, String token, Object object, Class responseClass) throws Exception {
        Log.w("TOKEN", appendBearer(token));
        Response<ResponseBody> bodyResponse = null;
        try {
            bodyResponse = mApi.postObjectBody(appendBearer(token), url, object).execute();
        } catch (Exception e) {
            throwException(e);
        }
        Log.w("token", token);
        Log.w("REPONSE", String.format("ResponseCode: %s", bodyResponse.code()));
        Log.w("REPONSE", String.format("Response: %s", bodyResponse.message()));
        Log.w("REPONSE", bodyResponse.raw() == null ? "Raw response is null" : bodyResponse.raw().request().url().toString());
        return checkResponse(responseClass, bodyResponse);
    }

    private Object checkResponse(Class responseClass, Response<ResponseBody> bodyResponse) throws Exception {
        if (bodyResponse.isSuccessful()) {
            return decodeResponse(responseClass, bodyResponse);
        } else {
            return throwFailError(bodyResponse);
        }
    }


    private Callback<ResponseBody> getResponse(final Class responseClass, final AppCallback callback) {

        return new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (callback != null) {
                    if (response.isSuccessful()) {
                        callback.onCompleted(decodeResponse(responseClass, response), "");
                    } else {
                        ApiError error = ServiceErrorUtil.parseError(response);
                        String message = error.getMessage();
                        if (error.getStatusCode() != 0 && error.getStatusCode() == 500) {
                            callback.onCompleted(null, SERVER_FAILED);
                        } else if (message != null && !message.isEmpty() && message != "No message available") {
                            callback.onCompleted(null, error.getMessage());
                        } else {
                            callback.onCompleted(null, GENERIC_ERROR);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (callback != null) {
                    String errorDesc = ServiceErrorUtil.handleThrowableError(t);
                    callback.onCompleted(null, errorDesc);
                }
            }
        };
    }


    private Object throwFailError(Response<ResponseBody> bodyResponse) throws Exception {
        ApiError error = ServiceErrorUtil.parseError(bodyResponse);
        if (error.getMessage() != null && !error.getMessage().equals("No message available")) {
            if (error.getStatusCode() == 500) {
                throw new Exception(SERVER_FAILED);
            } else {
                throw new Exception(error.getMessage());
            }

        } else {
            throw new Exception(GENERIC_ERROR);
        }
    }

    public Object throwException(Exception ex) {
        if (ex instanceof HttpStatusCodeException) {
            return ServiceErrorUtil.throwStatusCodeException((HttpStatusCodeException) ex);
        } else {
            return ServiceErrorUtil.throwRuntimeException(ex);
        }
    }


    private Object decodeResponse(Class reponseClass, Response<ResponseBody> bodyResponse) {
        Gson gson = new Gson();
        Object object = gson.fromJson(bodyResponse.body().charStream(), reponseClass);
        return object;
    }

    private String appendBearer(String token) {
        return token != null ? String.format("Bearer %s", token) : null;
    }

}
