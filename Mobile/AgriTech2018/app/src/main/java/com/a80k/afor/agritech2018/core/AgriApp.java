package com.a80k.afor.agritech2018.core;

import android.app.Application;
import android.content.Context;

public class AgriApp extends Application {



    private static Context sContext;

    public static Context getContext() {
        return sContext;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
    }
}
