package com.a80k.afor.agritech2018.core.retrofit.ws;

import com.a80k.afor.agritech2018.core.AppCallback;

/**
 * Created by Jan Paolo Regalado on 3/15/18.
 */

public interface Service {

    void getAsync(String url, String token, Class response, AppCallback callback);

    void postAsync(String url, String token, Object request, Class response, AppCallback callback);

    Object postStringRequest(String url, String token, String request, Class response) throws Exception;

    Object postObjectRequest(String url, String token, Object object, Class response) throws Exception;
}
