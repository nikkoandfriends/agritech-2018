package com.a80k.afor.agritech2018.data.product.logic;

import com.a80k.afor.agritech2018.core.AppCallback;
import com.a80k.afor.agritech2018.core.AppPreference;
import com.a80k.afor.agritech2018.core.auth.AuthLogicImpl;
import com.a80k.afor.agritech2018.core.retrofit.ws.Service;
import com.a80k.afor.agritech2018.data.product.data.History;
import com.a80k.afor.agritech2018.data.product.data.Product;
import com.a80k.afor.agritech2018.data.product.data.ProductRequest;

import org.springframework.http.ResponseEntity;

public class ProductLogicImpl extends AuthLogicImpl implements ProductLogic {

    private static ProductLogic INSTANCE;

    public ProductLogicImpl(Service mApi) {
        super(mApi);
    }

    public static ProductLogic getInstance(Service service){
        if(INSTANCE == null){
            INSTANCE = new ProductLogicImpl(service);
        }
        return INSTANCE;
    }

    @Override
    public void downloadProducts(final AppCallback callback) {
        getToken(new AppCallback() {
            @Override
            public void onCompleted(Object response, String message) {
                if(message.isEmpty()){
                    String token = (String) response;
                    mApi.getAsync(GET_PRODUCTS, token, Product[].class, callback);
                }else {
                    callback.onCompleted(response, message);
                }
            }
        });
    }

    @Override

    public void downloadHistories(final AppCallback callback) {
        getToken(new AppCallback() {
            @Override
            public void onCompleted(Object response, String message) {
                if(message.isEmpty()){
                    String token = (String) response;
                    mApi.getAsync(GET_HISTORY+"?userId="+ AppPreference.getUserId(), token, History[].class, callback);
                }else {
                    callback.onCompleted(response, message);
                }
            }
        });
    }

    @Override
    public void buyItem(final ProductRequest request, final AppCallback callback) {
        getToken(new AppCallback() {
            @Override
            public void onCompleted(Object response, String message) {
                if(message.isEmpty()){
                    String token = (String) response;
                    mApi.postAsync(BUY_ITEM, token,request, ResponseEntity.class, callback);
                }else {
                    callback.onCompleted(response, message);
                }
            }
        });
    }
}
