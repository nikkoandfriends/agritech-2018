package com.a80k.afor.agritech2018.core;

import android.content.Context;
import android.content.SharedPreferences;

public class AppPreference {

    private static final String PREF = "com.a80k.afor.agritech2018.core.PREF";
    private static final String TOKEN =  "com.a80k.afor.agritech2018.core.TOKEN";
    private static final String REMEMBERED_UNAME = "com.a80k.afor.agritech2018.core..REMEMBERED_UNAME";
    private static final String REMEMBERED_PASS = "com.a80k.afor.agritech2018.core.REMEMBERED_PASS";
    private static final String IS_LOGGEDIN = "com.a80k.afor.agritech2018.core.IS_LOGGEDIN";
    private static final String PARAM_TOKEN_AGE_KEY = "com.a80k.afor.agritech2018.core.PARAM_TOKEN_AGE_KEY";
    private static final String USER_EMAIL = "com.a80k.afor.agritech2018.core.USER_EMAIL";
    private static final String NAME = "com.a80k.afor.agritech2018.core.NAME";
    private static final String USER_ID = "com.a80k.afor.agritech2018.core.USER_ID";


    private static final String UNION_TOKEN ="com.a80k.afor.agritech2018.core.UNION_TOKEN";


    public synchronized static SharedPreferences getPref() {
        return AgriApp.getContext().getSharedPreferences(PREF, Context.MODE_PRIVATE);
    }

    public static String getUnionToken() {
        return getPref().getString(UNION_TOKEN, "");
    }

    public static void setUnionToken(String token) {
        getPref().edit().putString(UNION_TOKEN, token).commit();
    }

    public static String getToken() {
        return getPref().getString(TOKEN, "");
    }

    public static void setToken(String token) {
        getPref().edit().putString(TOKEN, token).commit();
    }


    public static String getRememberedPass() {
        return getPref().getString(REMEMBERED_PASS, "");
    }

    public static void setRememberedPass(String pass) {
        getPref().edit().putString(REMEMBERED_PASS, pass).commit();
    }

    public static boolean isLoggedIn() {
        return getPref().getBoolean(IS_LOGGEDIN, false);
    }

    public static void setLoggedIn(boolean isLoggedIn) {
        getPref().edit().putBoolean(IS_LOGGEDIN, isLoggedIn).commit();
    }

    public static String getRememberedUName() {
        return getPref().getString(REMEMBERED_UNAME, "");
    }


    public static void setRememberedUname(String uName) {
        getPref().edit().putString(REMEMBERED_UNAME, uName).commit();
    }

    public static long getTokenAge() {
        return getPref().getLong(PARAM_TOKEN_AGE_KEY, -1L);
    }

    public static void setTokenAge(long value) {
        getPref().edit().putLong(PARAM_TOKEN_AGE_KEY, value).commit();
    }

    public static String getUserEmail() {
        return getPref().getString(USER_EMAIL, "");
    }

    public static void setUserEmail(String email) {
        getPref().edit().putString(USER_EMAIL, email).commit();
    }

    public static String getName() {
        return getPref().getString(NAME, "");
    }

    public static void setName(String name) {
        getPref().edit().putString(NAME, name).commit();
    }

    public static String getUserId() {
        return getPref().getString(USER_ID, "");
    }

    public static void setUserId(String userId) {
        getPref().edit().putString(USER_ID, userId).commit();
    }
}
