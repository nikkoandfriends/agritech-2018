package com.a80k.afor.agritech2018.user;

import com.a80k.afor.agritech2018.core.AppCallback;
import com.a80k.afor.agritech2018.core.auth.AuthLogicImpl;
import com.a80k.afor.agritech2018.core.retrofit.ws.Service;

public class UserLogicImpl extends AuthLogicImpl implements UserLogic {

    private static UserLogic INSTANCE;

    public static UserLogic getINSTANCE(Service service) {
        if (INSTANCE == null) {
            INSTANCE = new UserLogicImpl(service);
        }
        return INSTANCE;
    }

    public UserLogicImpl(Service mApi) {
        super(mApi);
    }


    @Override
    public void register(final User user, final AppCallback callback) {
        mApi.postAsync(REGISTER_URL, "", user, User.class, callback);
    }
}
