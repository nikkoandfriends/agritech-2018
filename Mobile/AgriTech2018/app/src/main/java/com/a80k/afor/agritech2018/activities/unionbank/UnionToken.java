package com.a80k.afor.agritech2018.activities.unionbank;

import android.os.Parcel;
import android.os.Parcelable;

public class UnionToken implements Parcelable{
    private String token_type;
    private String access_token;
    private String metadata;
    private long expires_in;
    private String scope;
    private String refresh_token;

    protected UnionToken(Parcel in) {
        token_type = in.readString();
        access_token = in.readString();
        metadata = in.readString();
        expires_in = in.readLong();
        scope = in.readString();
        refresh_token = in.readString();
    }

    public static final Creator<UnionToken> CREATOR = new Creator<UnionToken>() {
        @Override
        public UnionToken createFromParcel(Parcel in) {
            return new UnionToken(in);
        }

        @Override
        public UnionToken[] newArray(int size) {
            return new UnionToken[size];
        }
    };

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public long getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(long expires_in) {
        this.expires_in = expires_in;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(token_type);
        parcel.writeString(access_token);
        parcel.writeString(metadata);
        parcel.writeLong(expires_in);
        parcel.writeString(scope);
        parcel.writeString(refresh_token);
    }
}
