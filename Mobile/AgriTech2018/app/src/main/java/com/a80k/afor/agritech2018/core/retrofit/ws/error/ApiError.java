package com.a80k.afor.agritech2018.core.retrofit.ws.error;

/**
 * Created by Jan Paolo Regalado on 3/15/18.
 */

public class ApiError {

    private int status;
    private String message;

    public int getStatusCode() {
        if (status == 0) {
            return 500;
        }
        return status;
    }

    public void setStatusCode(int statusCode) {
        this.status = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
