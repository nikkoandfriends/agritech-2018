package com.a80k.afor.agritech2018.data.product.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bautista on 9/16/2018.
 */

public class UnionBankTransfer implements Parcelable{
    private String createdAt;
    private String senderTransferId;
    private String state;
    private String tranId;

    protected UnionBankTransfer(Parcel in) {
        createdAt = in.readString();
        senderTransferId = in.readString();
        state = in.readString();
        tranId = in.readString();
    }

    public static final Creator<UnionBankTransfer> CREATOR = new Creator<UnionBankTransfer>() {
        @Override
        public UnionBankTransfer createFromParcel(Parcel in) {
            return new UnionBankTransfer(in);
        }

        @Override
        public UnionBankTransfer[] newArray(int size) {
            return new UnionBankTransfer[size];
        }
    };

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getSenderTransferId() {
        return senderTransferId;
    }

    public void setSenderTransferId(String senderTransferId) {
        this.senderTransferId = senderTransferId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTranId() {
        return tranId;
    }

    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(createdAt);
        dest.writeString(senderTransferId);
        dest.writeString(state);
        dest.writeString(tranId);
    }
}
