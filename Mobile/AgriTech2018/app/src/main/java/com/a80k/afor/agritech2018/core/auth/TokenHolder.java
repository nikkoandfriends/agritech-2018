package com.a80k.afor.agritech2018.core.auth;

import android.os.Parcel;
import android.os.Parcelable;

class TokenHolder implements Parcelable {
    private String token;

    protected TokenHolder(Parcel in) {
        token = in.readString();
    }

    public static final Creator<TokenHolder> CREATOR = new Creator<TokenHolder>() {
        @Override
        public TokenHolder createFromParcel(Parcel in) {
            return new TokenHolder(in);
        }

        @Override
        public TokenHolder[] newArray(int size) {
            return new TokenHolder[size];
        }
    };

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(token);
    }
}

