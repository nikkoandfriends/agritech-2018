package com.a80k.afor.agritech2018.user;

import com.a80k.afor.agritech2018.core.AppCallback;

public interface UserLogic {

    String REGISTER_URL = "/api/user/register";

    void register(User user, AppCallback callback);
}
