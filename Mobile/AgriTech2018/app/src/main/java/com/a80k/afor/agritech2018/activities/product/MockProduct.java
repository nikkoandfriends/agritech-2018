package com.a80k.afor.agritech2018.activities.product;

import com.a80k.afor.agritech2018.data.product.data.Product;
import com.a80k.afor.agritech2018.data.product.data.ProductType;

import java.util.ArrayList;
import java.util.List;

public class MockProduct {

    public static List<Product> getProductList(){

        Product product = new Product();
        product.setName("Fertilizer 1");
        product.setDescription("A very good fertilizer that is so good");
        product.setPrice(130);
        product.setType(ProductType.FERTILIZER);

        Product product2 = new Product();
        product2.setName("Fertilizer 1");
        product2.setDescription("A very good fertilizer that is so good");
        product2.setPrice(130);
        product2.setType(ProductType.FERTILIZER);

        Product product3 = new Product();
        product3.setName("Fertilizer 2");
        product3.setDescription("A very good fertilizer that is so good");
        product3.setPrice(130);
        product3.setType(ProductType.FERTILIZER);

        Product product4 = new Product();
        product4.setName("Fertilizer 3");
        product4.setDescription("A very good fertilizer that is so good");
        product4.setPrice(130);
        product4.setType(ProductType.FERTILIZER);

        Product product5 = new Product();
        product5.setName("Fertilizer 4");
        product5.setDescription("A very good fertilizer that is so good");
        product5.setPrice(130);
        product5.setType(ProductType.FERTILIZER);

        Product product6 = new Product();
        product6.setName("Fertilizer 5");
        product6.setDescription("A very good fertilizer that is so good");
        product6.setPrice(130);
        product6.setType(ProductType.FERTILIZER);

        List<Product> products = new ArrayList<>();
        products.add(product);
        products.add(product2);
        products.add(product3);
        products.add(product4);
        products.add(product5);
        products.add(product6);
        return products;

    }
}
