package com.a80k.afor.agritech2018.data.product.data;

public enum  ProductType {

    FERTILIZER, PESTICIDE
}
