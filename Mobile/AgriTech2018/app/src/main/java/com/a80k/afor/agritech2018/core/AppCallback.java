package com.a80k.afor.agritech2018.core;

public interface AppCallback {
    void onCompleted(Object response, String message);
}