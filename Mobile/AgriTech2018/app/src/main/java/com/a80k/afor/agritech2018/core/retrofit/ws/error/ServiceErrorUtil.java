package com.a80k.afor.agritech2018.core.retrofit.ws.error;


import com.a80k.afor.agritech2018.core.retrofit.ws.RestUrlHelper;
import com.a80k.afor.agritech2018.core.retrofit.ws.ServiceApiImpl;

import org.springframework.web.client.HttpStatusCodeException;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by Jan Paolo Regalado on 3/15/18.
 */

public class ServiceErrorUtil {

    public static ApiError parseError(Response<?> response) {
        Converter<ResponseBody, ApiError> converter =
                RestUrlHelper.getInstance().buildRetro()
                        .responseBodyConverter(ApiError.class, new Annotation[0]);

        ApiError error;

        try {
            error = converter.convert(response.errorBody());
            return error;
        } catch (Exception e) {
            return new ApiError();
        }
    }

    public static String handleThrowableError(Throwable t) {
        String message = t.getMessage();
        if (t instanceof ConnectException || t instanceof UnknownHostException || t instanceof SocketTimeoutException) {
            message = ServiceApiImpl.SERVER_FAILED;
        } else if (t instanceof IllegalStateException) {
            message = String.valueOf(t.getCause());
        } else {
            t.getLocalizedMessage();
        }
        return message;
    }

    public static Object throwStatusCodeException(HttpStatusCodeException ex) throws RuntimeException {
        HttpStatusCodeException error = ex;
        String responseString = error.getResponseBodyAsString();
        ApiError apiError = parseStringError(error.getResponseBodyAsString());
        if (apiError.getMessage() != null && !apiError.getMessage().isEmpty()) {
            throw new RuntimeException(apiError.getMessage());
        } else if (responseString != null && !responseString.isEmpty()) {
            throw new RuntimeException(responseString);
        } else {
            throw new RuntimeException(ServiceApiImpl.GENERIC_ERROR);
        }
    }


    private static ApiError parseStringError(String string) {
        ApiError apiError = new ApiError();
        List<String> stringList = Arrays.asList(string.split(","));
        for (String str : stringList) {
            if (str.contains("status")) {
                apiError.setStatusCode(Integer.valueOf(str.substring(10)));
            }
            if (str.contains("message")) {
                String message = str.substring(10);
                message = message.replaceAll("^\"|\"$", "");
                apiError.setMessage(message);
            }
        }
        return apiError;
    }

    public static Object throwRuntimeException(Exception ex) {
        if (ex.getMessage() == null) {
            throw new RuntimeException(ServiceApiImpl.GENERIC_ERROR);
        } else if (ex.getMessage() != null && ex.getMessage().contains("SocketTimeout")) {
            throw new RuntimeException("Failed to connect to server. Connection timeout.");
        } else if ((ex.getMessage() != null && ex.getMessage().contains("http"))
            /*||( ex instanceof ResourceAccessException && ex.getCause() instanceof HttpHostConnectException)*/) {
            throw new RuntimeException("Server Maintenance, Please try again later or try upgrading POS app.");
        } else if (ex.getCause() != null && ex.getCause() instanceof UnknownHostException) {
            throw new RuntimeException(ServiceApiImpl.GENERIC_ERROR);
        } else {
            throw new RuntimeException(ServiceApiImpl.GENERIC_ERROR);
        }
    }
}
