package com.a80k.afor.agritech2018.core.retrofit.ws;

import com.a80k.afor.agritech2018.BuildConfig;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by OJGarde | ojgarde@satellite.com.ph
 * on 08/08/2016.
 */
public class RestUrlHelper{
    private static RestUrlHelper INSTANCE;
    private static Retrofit.Builder builder = null;

    private RestUrlHelper() {
    }

    public static RestUrlHelper getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RestUrlHelper();
        }
        return INSTANCE;
    }


    public <T> T createAppApiInstance(Class<T> responseClass) {
        createBuilder(BuildConfig.API_URL);

        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient())
                .build().create(responseClass);
    }

    private void createBuilder(String url) {
        builder = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create());
    }

    private OkHttpClient okHttpClient() {
        OkHttpClient okClient = new OkHttpClient.Builder()
                .addInterceptor(
                        new Interceptor() {
                            @Override
                            public Response intercept(Chain chain) throws IOException {
                                Request original = chain.request();

                                Request.Builder requestBuilder = original.newBuilder()
                                        .header("Accept", "application/json")
                                        .method(original.method(), original.body());

                                Request request = requestBuilder.build();
                                return chain.proceed(request);
                            }
                        })
                .build();
        return okClient;
    }

    public Retrofit buildRetro() {
        return builder.build();
    }
}
