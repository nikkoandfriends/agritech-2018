package com.a80k.afor.agritech2018.activities.product;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.a80k.afor.agritech2018.R;
import com.a80k.afor.agritech2018.core.AppCallback;
import com.a80k.afor.agritech2018.core.factory.AppFactory;
import com.a80k.afor.agritech2018.data.product.data.Product;

import java.util.Arrays;
import java.util.List;


public class ProductsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView recyclerView;
    private List<Product> productList;
    private SwipeRefreshLayout swipeLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.products_activity);
        swipeLayout = findViewById(R.id.swipe);
        swipeLayout.setOnRefreshListener(this);
        initRecycler();
//        productList = MockProduct.getProductList();
        initRecycler();
        initRecyclerView();

        enableBackButton();

        downloadProducts();

    }

    private void downloadProducts() {
        swipeLayout.setRefreshing(true);
        AppFactory.getProducLogic().downloadProducts(new AppCallback() {
            @Override
            public void onCompleted(Object response, String message) {
                if (ProductsActivity.this != null) {
                    swipeLayout.setRefreshing(false);
                    if (message.isEmpty()) {
                        Product[] products = (Product[]) response;
                        productList = Arrays.asList(products);
                        initRecyclerView();
                    } else
                        Toast.makeText(ProductsActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }
            case R.id.to_history: {
                redirectToHistory();
                break;
            }
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.market_menu, menu);
        return super.onCreateOptionsMenu(menu);

    }

    private void enableBackButton() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    private void initRecyclerView() {
        ProductAdapter adapter = new ProductAdapter(productList, ProductsActivity.this, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Product product = (Product) view.getTag();
                Intent intent = new Intent(ProductsActivity.this, ProductActivity.class);
                intent.putExtra(ProductActivity.PRODUCT_EXTRA, product);
                startActivity(intent);
            }
        });

        recyclerView.setAdapter(adapter);
    }

    private void initRecycler() {
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
    }

    private void redirectToHistory() {
        startActivity(new Intent(this, HistoriesActivity.class));
    }

    @Override
    public void onRefresh() {
        downloadProducts();
    }
}
